import tensorflow as tf
import numpy as np
from tensorflow import keras
import random as rdm
import math as mt
import matplotlib.pyplot as plt
import os
# Permet de supprimer un warning disant qu'on optimise pas les opérations (AVX2) du CPU.
# Mais pas important car on utilise le GPU :)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class CircleGenerator :
    def __init__(self, nb_circles, ecart):
        self.nb_circles = nb_circles
        self.circlesCenters = []
        self.circlesRadius = []
        self.ecart = ecart
        for i in range(nb_circles):
            x = rdm.uniform(3, 10)
            y = rdm.uniform(3, 10)
            r = rdm.uniform(3, 10)
            self.circlesCenters.append((x, y))
            self.circlesRadius.append(r)
            print("Cercle : (", x, ", ", y, "), r = ", r)

    def get_point(self):
        id_cercle = rdm.randint(0, self.nb_circles - 1)
        theta = rdm.uniform(0, 1) * 360

        # On prend un point du cercle
        x, y = self.circlesCenters[id_cercle]
        x += self.circlesRadius[id_cercle] * mt.cos(theta)
        y += self.circlesRadius[id_cercle] * mt.sin(theta)

        # On rajoute l'erreur
        phi = rdm.uniform(0, 1) * 360
        r = rdm.uniform(0, 1) * self.ecart
        x += r * mt.cos(phi)
        y += r * mt.sin(phi)

        return x, y, id_cercle

    def get_color(self, indice):
        colors = ['blue', 'orange', 'green', 'red', 'cyan', 'yellow', 'black', 'magenta']
        return colors[indice % len(colors)]

    def plot_circles(self):
        thetas = [x / 180 * mt.pi for x in range(360)]
        for i in range(self.nb_circles):
            x = [self.circlesCenters[i][0] + self.circlesRadius[i] * mt.cos(theta) for theta in thetas]
            y = [self.circlesCenters[i][1] + self.circlesRadius[i] * mt.sin(theta) for theta in thetas]
            plt.plot(x, y, color=self.get_color(i))

    @staticmethod
    def plot_points(points):
        for i in range(cg.nb_circles):
            pts = [x for x in points if x[2] == i]
            x = [x[0] for x in pts]
            y = [x[1] for x in pts]
            color = cg.get_color(i)
            plt.scatter(x, y, c=color)

    @staticmethod
    def plot_points_with_label(points, labels):
        for (x, y, res), label in zip(points, labels):
            if res == label:
                plt.scatter(x, y, c="black")
            else:
                plt.scatter(x, y, c=cg.get_color(label))

    @staticmethod
    def get_repartition(axes):
        x1, x2, y1, y2 = axes
        freq = 0.5
        nb_x = int((x2 - x1) / freq)
        nb_y = int((y2 - y1) / freq)
        x = [x1 + i * freq for i in range(nb_x + 1)]
        x = [x] * (nb_y + 1)
        y = [[y1 + i * freq] * (nb_x + 1) for i in range(nb_y + 1)]
        return x, y

    def plot_repartition(self, points, labels):
        for (x, y), label in zip(points, labels):
            plt.scatter(x, y, c=cg.get_color(label))




cg = CircleGenerator(5, 0.5)

points = [cg.get_point() for x in range(100000)]
# points = [cg.get_point() for x in range(100)]
plt.figure(figsize=(15, 5))
plt.subplot(1, 3, 1)
cg.plot_circles()
CircleGenerator.plot_points(points)
# plt.ion()
# plt.show()
# plt.pause(0.001)

model = tf.keras.models.Sequential([tf.keras.layers.Flatten(input_shape=[2]),
                                   tf.keras.layers.Dense(126, activation=tf.nn.relu),
                                   tf.keras.layers.Dense(cg.nb_circles, activation=tf.nn.softmax)])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

datas = np.array([np.array([x[0], x[1]]) for x in points], dtype=float)
labels = np.array([x[2] for x in points], dtype=float)
model.fit(x=datas, y=labels, epochs=10, verbose=1)

points_test = [cg.get_point() for x in range(1000)]
datas_test = np.array([np.array([x[0], x[1]]) for x in points_test])
labels_test = np.array([x[2] for x in points_test])
labels = model.predict(x=datas_test, verbose=1)
labels = [np.argmax(x) for x in labels]

plt.subplot(1, 3, 2)
cg.plot_circles()
CircleGenerator.plot_points_with_label(points_test, labels)

plt.subplot(1, 3, 3)
pts_x, pts_y = CircleGenerator.get_repartition([-5, 20, -5, 20])
pts_x, pts_y = np.array(pts_x).flatten(), np.array(pts_y).flatten()
points_repartition = np.array([[x, y] for x, y in zip(pts_x, pts_y)])
labels_repartition = model.predict(x=points_repartition, verbose=1)
labels_repartition = [np.argmax(x) for x in labels_repartition]
cg.plot_circles()
cg.plot_repartition(points_repartition, labels_repartition)

plt.show()
