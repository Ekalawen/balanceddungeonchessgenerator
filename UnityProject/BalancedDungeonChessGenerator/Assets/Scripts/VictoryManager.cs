﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VictoryConditions {
    FIRST_KING_BLOOD // Ce sera toujours ce mode là pour le moment ! :D
    //LAST_KING_BLOOD,
    //CAPTURE_THE_POINT
}

public class VictoryManager {

    private GameManager gm;
    private ChessResult lastResult;
    private int nbGames = 0;
    private int nbWinWhite = 0;
    private int nbWinBlack = 0;

    public VictoryManager(GameManager gm_) {
        gm = gm_;
    }

    public bool IsOver(ChessColor currentColor) {
        if (gm.turnNumber >= gm.nbMaxTurns) {
            lastResult = ChessResult.TIED;
            return true;
        }

        List<PieceData> pieces = gm.pieceManager.GetAllPieceOfColor(currentColor);

        bool noValidMove = true;
        foreach(PieceData p in pieces) {
            if(p.HasValidMove()) {
                noValidMove = false;
                break;
            }
        }

        if (noValidMove)
            lastResult = (ChessResult)gm.OtherColor(currentColor);

        return noValidMove;
    }

    public void PrintVictory() {
        nbGames++;
        if (lastResult != ChessResult.TIED) {
            Debug.Log("Le joueur " + (ChessColor)lastResult + " a gagné !");
            nbWinWhite += (lastResult == ChessResult.WHITE_WIN) ? 1 : 0;
            nbWinBlack += (lastResult == ChessResult.BLACK_WIN) ? 1 : 0;
        } else {
            Debug.Log("Il y a eu ÉGALITÉ !");
        }
    }

    public void PrintRatios() {
        float whiteRatio = (float)nbWinWhite / (float)nbGames * 100.0f;
        float blackRatio = (float)nbWinBlack / (float)nbGames * 100.0f;
        float egaliteRatio = (float)(nbGames - nbWinWhite - nbWinBlack) / (float)nbGames * 100.0f;
        Debug.Log("Sur " + nbGames + " parties :\n" +
            "BLANC a gagné " + whiteRatio + "% \n" +
            "NOIR a gagné " + blackRatio + "% \n" +
            "Il y a eus ÉGALITÉ " + egaliteRatio + "%");
    }

    public bool IsLostForPiecesPositions(List<PieceData> newPieces, ChessColor currentColor) {
        // On sauvegarde les pièces du pieceManager
        List<PieceData> savePieces = gm.pieceManager.piecesDatas;
        gm.pieceManager.piecesDatas = newPieces;

        bool isLost = IsOver(currentColor);

        // Puis on les restaures
        gm.pieceManager.piecesDatas = savePieces;

        return isLost;
    }

    public ChessResult GetGameResult() {
        return lastResult;
    }

    public bool IsTimeOut(int currentTurn) {
        return currentTurn >= gm.nbMaxTurns;
    }
}
