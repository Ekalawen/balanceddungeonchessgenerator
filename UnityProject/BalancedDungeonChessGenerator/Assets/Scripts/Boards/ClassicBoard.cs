﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicBoard : Board {

    protected override void InitializeMap() {
        map = new List<List<Tile>>(size);
        for(int i = 0; i < size; i++) {
            map.Add(new List<Tile>(size));
            for(int j = 0; j < size; j++) {
                Tile tile = new Tile();
                tile.type = TileType.EMPTY;
                tile.offsetY = 0.0f;
                map[i].Add(tile);
            }
        }
    }

}
