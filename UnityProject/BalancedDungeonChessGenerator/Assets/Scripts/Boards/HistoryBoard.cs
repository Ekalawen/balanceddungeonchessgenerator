﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoryBoard : Board {

    protected override void InitializeMap() {
        size = gm.history.data.map.Count;
        map = new List<List<Tile>>();
        foreach(TileList tl in gm.history.data.map) {
            List<Tile> row = new List<Tile>();
            map.Add(row);
            foreach(Tile t in tl.list) {
                row.Add(t);
            }
        }
    }
}
