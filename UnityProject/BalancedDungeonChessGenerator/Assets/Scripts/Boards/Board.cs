﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType { EMPTY, MOUNTAIN, FOREST, DUNGEON}; // We will start with just the simplest concepts !

[System.Serializable]
public struct TileList {
    public List<Tile> list;
}

[System.Serializable]
public class Tile {
    public TileType type;
    public float offsetY;
    public void SetOffsetY(float offset) {
        offsetY = offset;
    }
}

public class Board : MonoBehaviour
{

    public int size = 8;
    public float offsetY = 0.1f;

    public GameObject tileObject;
    public Material whiteMaterial;
    public Material blackMaterial;
    public GameObject mountainObject;
    public GameObject forestObject;
    public GameObject dungeonObject;

    private GameObject mapContainer;
    private GameObject tileContainer;
    private GameObject decorContainer;
    [HideInInspector]
    public List<List<Tile>> map;
    protected GameManager gm;
    public GameObject[,] tiles;


    public void Initialize() {
        gm = FindObjectOfType<GameManager>();

        CreateHierarchy();

        InitializeMap();

        if(gm.display)
            DisplayMap();
    }

    private void CreateHierarchy() {
        mapContainer = new GameObject("Map");
        tileContainer = new GameObject("Tile");
        decorContainer = new GameObject("Decor");
        tileContainer.transform.SetParent(mapContainer.transform);
        decorContainer.transform.SetParent(mapContainer.transform);
    }

    protected virtual void InitializeMap() {
        map = new List<List<Tile>>(size);
        for(int i = 0; i < size; i++) {
            map.Add(new List<Tile>(size));
            for(int j = 0; j < size; j++) {
                Tile tile = new Tile();
                tile.type = (TileType)(Random.Range(0, System.Enum.GetValues(typeof(TileType)).Length));
                tile.offsetY = 0.0f;
                map[i].Add(tile);
            }
        }
    }

    private void DisplayMap() {
        tiles = new GameObject[size, size];
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                DisplayTile(i, j);
            }
        }
    }

    private void DisplayTile(int i, int j) {
        // Base of the tile
        float offset = Random.Range(-offsetY, offsetY);
        GameObject go = Instantiate(tileObject, new Vector3(i, offset, j), Quaternion.identity);
        Material mat = ((i + j) % 2 == 0) ? blackMaterial : whiteMaterial;
        go.GetComponent<Renderer>().material = mat;
        go.transform.SetParent(tileContainer.transform);
        tiles[i, j] = go;
        map[i][j].SetOffsetY(offset);

        // Its type that we put over it
        GameObject model = null;
        switch(map[i][j].type)
        {
            case TileType.EMPTY:
                return;
            case TileType.FOREST:
                model = forestObject;
                break;
            case TileType.MOUNTAIN:
                model = mountainObject;
                break;
            case TileType.DUNGEON:
                model = dungeonObject;
                break;
        }
        go = Instantiate(model, new Vector3(i, offset + 0.5f, j), Quaternion.identity);
        go.transform.SetParent(decorContainer.transform);
    }

    public Vector3 GetCenter() {
        return new Vector3(size / 2.0f, 0.0f, size / 2.0f);
    }

    public bool IsIn(Vector2Int position) {
        return position.x >= 0 && position.y >= 0 && position.x < size && position.y < size;
    }

    public bool IsWalkable(Vector2Int position, bool noConsiderMarked = false) {
        Tile t = map[position.x][position.y];
        bool positionBloquee = gm.pieceManager.IsPiece(position, noConsiderMarked);
        return t.type != TileType.FOREST && t.type != TileType.MOUNTAIN && !positionBloquee;
    }

    public bool IsStayable(Vector2Int position, bool noConsiderMarked = false) {
        Tile t = map[position.x][position.y];
        bool positionBloquee = gm.pieceManager.IsPiece(position, noConsiderMarked);
        return t.type != TileType.MOUNTAIN && !positionBloquee;
    }

    public bool IsCapturable(Vector2Int position, ChessColor color, bool noConsiderMarked = false) {
        Tile t = map[position.x][position.y];
        bool positionBloquee = gm.pieceManager.IsPiece(position, noConsiderMarked);
        if (positionBloquee) {
            PieceData otherPiece = gm.pieceManager.GetPiece(position, noConsiderMarked);
            return t.type != TileType.MOUNTAIN && otherPiece.color != color;
        } else {
            return t.type != TileType.MOUNTAIN;
        }
    }

    public List<Vector2Int> AllStayablePositions() {
        List<Vector2Int> positions = new List<Vector2Int>();
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                Vector2Int pos = new Vector2Int(i, j);
                if(IsStayable(pos)) {
                    positions.Add(pos);
                }
            }
        }
        return positions;
    }

    public bool IsValid(Play play, Player player) {
        PieceData piece = FindObjectOfType<GameManager>().pieceManager.GetPiece(play.from);
        bool canGo = piece.CanGoTo(play.to) && piece.color == player.color;
        bool kingsSafe = gm.pieceManager.IsKingSafe(play);
        return canGo && kingsSafe;
    }
}
