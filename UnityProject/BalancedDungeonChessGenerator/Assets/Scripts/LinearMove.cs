﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearMove : MonoBehaviour {

    public bool canCapture = true;
    public bool mustCapture = false;
    public bool firstMoveOnly = false;

    protected Board board;
    protected PieceManager pieceManager;
    [SerializeField]
    public Piece piece;

    public int distance = int.MaxValue;
    public List<Vector2Int> directions;
    public bool circularMove = true;

    public LinearMoveData data;

    public void Initialize() {
        board = FindObjectOfType<GameManager>().board;
        pieceManager = FindObjectOfType<GameManager>().pieceManager;
        piece = GetComponent<Piece>();
        OnStart();
    }

    public static LinearMove CreateAndAttachLinearMove(LinearMoveData lmd, GameObject go) {
        LinearMove lm = go.AddComponent<LinearMove>();
        lm.canCapture = lmd.canCapture;
        lm.mustCapture = lmd.mustCapture;
        lm.firstMoveOnly = lmd.firstMoveOnly;
        lm.distance = lmd.distance;
        lm.directions = lmd.directions;
        lm.circularMove = lmd.circularMove;
        lm.Initialize();
        return lm;
    }

    protected void OnStart() {
        distance = Mathf.Min(distance, board.size);
        AddCircularMove();
        data = new LinearMoveData(canCapture,
                                  mustCapture,
                                  firstMoveOnly,
                                  distance,
                                  directions,
                                  circularMove,
                                  piece.GetData());
    }

    private void AddCircularMove() {
        if(circularMove) {
            List<Vector2Int> newDirections = new List<Vector2Int>();
            foreach(Vector2Int direction in directions) {
                Vector2Int forward = direction;
                newDirections.Add(forward);
                forward = LinearMove.RotateRight(forward);
                newDirections.Add(forward);
                forward = LinearMove.RotateRight(forward);
                newDirections.Add(forward);
                forward = LinearMove.RotateRight(forward);
                newDirections.Add(forward);
            }
            directions = newDirections;
        }
    }

    public static Vector2Int RotateRight(Vector2Int pos) {
        // (1, 2) ==> (2, -1)
        return new Vector2Int(pos.y, -pos.x);
    }
}
