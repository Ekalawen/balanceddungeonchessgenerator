﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float distanceMin = 0.25f;
    public float distanceMax = 3.25f;
    public float vitesseRotationY = 5.0f;
    public float vitesseRotationX = 5.0f;
    public float vitesseRapprochement = 5.0f;

    private Vector3 mapCenter;
    private float sizeMap;

    // Start is called before the first frame update
    void Start() {
        GameManager gm = FindObjectOfType<GameManager>();
        mapCenter = gm.board.GetCenter();
        sizeMap = gm.board.size;

        Vector3 direction = (Vector3.up - Vector3.forward).normalized;
        transform.position = mapCenter + gm.board.size * (distanceMin + distanceMax) / 2.0f * direction;
        transform.LookAt(mapCenter);
    }

    // Update is called once per frame
    void Update() {
        MoveCamera();
    }

    private void MoveCamera() {
        if(Input.GetMouseButton(1)) {
            Vector3 axe = Vector3.up;
            float angle = Time.deltaTime * vitesseRotationX * Input.GetAxis("Mouse X") * 180.0f / Mathf.PI;
            transform.RotateAround(mapCenter, axe, angle);

            axe = Vector3.Cross(Vector3.up, transform.position - mapCenter);
            angle = Time.deltaTime * vitesseRotationY * Input.GetAxis("Mouse Y") * 180.0f / Mathf.PI;
            Vector3 projection = Vector3.Project(transform.position - mapCenter, Vector3.up); // clamp to prevent to be at vertical
            if (Vector3.Distance(mapCenter + projection, transform.position) <= 1.0f) {
                angle = Mathf.Max(0, angle);
            }
            transform.RotateAround(mapCenter, axe, angle);
        }

        if(Input.GetAxis("Mouse ScrollWheel") != 0) {
            float distance = Vector3.Distance(mapCenter, transform.position);
            distance -= Input.GetAxis("Mouse ScrollWheel") * vitesseRapprochement;
            distance = Mathf.Clamp(distance, sizeMap * distanceMin, sizeMap * distanceMax);
            Vector3 vecteurDirecteur = transform.position - mapCenter;
            vecteurDirecteur.Normalize();
            transform.position = mapCenter + vecteurDirecteur * distance;
        }

        // clamp to prevent to go underground
        float yMin = (Vector3.Distance(mapCenter, transform.position) <= sizeMap * Mathf.Sqrt(2) / 2.0f) ? 2.0f : 0.0f;
        if(transform.position.y <= yMin) {
            Vector3 pos = transform.position;
            pos.y = yMin;
            transform.position = pos;
        }

        transform.LookAt(mapCenter);
    }
}
