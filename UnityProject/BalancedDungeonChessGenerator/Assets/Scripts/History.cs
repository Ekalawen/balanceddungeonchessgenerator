﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class HistoryPlay {
    public Play play;
    public PieceData pieceCaptured = null;
    public bool hasCapture = false;

    public HistoryPlay(Play play_, PieceData pieceCaptured_) {
        play = play_;
        pieceCaptured = pieceCaptured_;
        hasCapture = (pieceCaptured_ != null);
    }

    public HistoryPlay(Play play_, GameManager gm) {
        PieceData capturedPiece = gm.pieceManager.GetPiece(play_.to);
        play = play_;
        pieceCaptured = capturedPiece;
        hasCapture = (capturedPiece != null);
    }
}

[Serializable]
public struct HistoryData {
    public List<PieceModelData> whitePiecesModels;
    public List<PieceModelData> blackPiecesModels;
    public List<PieceData> pieces;
    public List<TileList> map;
    public List<HistoryPlay> plays;
}


public class History : MonoBehaviour {

    public float playRate = 0.5f;
    public Text textIndicePlay;

    [HideInInspector]
    public HistoryData data;

    private GameManager gm;
    private bool isPlaying = false;
    private Coroutine currentCoroutine;
    private int indicePlay = 0;

    public void SaveFirstState() {
        gm = FindObjectOfType<GameManager>();
        // Récupérer les variables importantes
        data.map = new List<TileList>();
        foreach (List<Tile> row in gm.board.map) {
            TileList tl;
            tl.list = row;
            data.map.Add(tl);
        }
        data.whitePiecesModels = new List<PieceModelData>();
        data.blackPiecesModels = new List<PieceModelData>();
        data.pieces = new List<PieceData>();
        foreach(Piece p in gm.pieceManager.whitePiecesModels) {
            PieceModelData pmd = p.GetModelData();
            //foreach(LinearMoveData lmd in pmd.data.GetMoves()) {
            //    lmd.AddCircularMove();
            //}
            data.whitePiecesModels.Add(pmd);
        }
        foreach(Piece p in gm.pieceManager.blackPiecesModels) {
            PieceModelData pmd = p.GetModelData();
            //foreach(LinearMoveData lmd in pmd.data.GetMoves()) {
            //    lmd.AddCircularMove();
            //}
            data.blackPiecesModels.Add(pmd);
        }
        foreach(PieceData p in gm.pieceManager.piecesDatas) {
            PieceData newData = p.GetCopy();
            data.pieces.Add(newData);
        }
        data.plays = new List<HistoryPlay>();
    }

    public void SaveNextState(HistoryPlay nextPlay) {
        data.plays.Add(nextPlay);
    }

    public void SaveHistory() {
        DateTime dt = DateTime.Now;
        string fileName = dt.ToString("MM-dd_HH-mm-ss") + "_replay_" + gm.sceneName + ".json";
        string path = Application.persistentDataPath + "/" + fileName;

        string json = JsonUtility.ToJson(data);

        StreamWriter sw = new StreamWriter(path);
        if(sw != null) {
            sw.Write(json);
            sw.Close();

            Debug.Log("Partie enregistré à : " + Application.persistentDataPath + "/\n Au nom : " + fileName);

        } else {
            throw new SystemException("Echec de chargement du streamwriter !" + path);
        }
    }

    public void LoadHistory(string historyPath) {
        gm = FindObjectOfType<GameManager>();
        string path = Application.persistentDataPath + "/" + gm.historyPath;
        StreamReader sr = new StreamReader(path);

        if (sr != null) {
            data = JsonUtility.FromJson<HistoryData>(sr.ReadToEnd());

        } else {
            throw new SystemException("Echec de chargement du streamreader !" + path);
        }
    }

    public void PlayUntilEnd() {
        isPlaying = true;
        PlayNextPlay();
        currentCoroutine = StartCoroutine(PlayInSeconds());
    }

    private IEnumerator PlayInSeconds() {
        yield return new WaitForSeconds(playRate);

        PlayNextPlay();

        if (isPlaying)
            currentCoroutine = StartCoroutine(PlayInSeconds());
    }

    public void StopPlaying() {
        isPlaying = false;
        if (currentCoroutine != null)
            StopCoroutine(currentCoroutine);
    }

    public void PlayNextPlay() {
        if(indicePlay < data.plays.Count) {
            gm.pieceManager.Play(data.plays[indicePlay].play);
            SetIndicePlay(indicePlay + 1);
            if (indicePlay == data.plays.Count)
                isPlaying = false;
        }
    }

    public void UndoLastPlay() {
        if(indicePlay > 0) {
            // Renvoyer la pièce à la position précédente, il faut donc inverser le play !
            Play play = data.plays[indicePlay - 1].play; // Putain si c'est pas une copie je tue le c# !
            Vector2Int tmp = play.from;
            play.from = play.to;
            play.to = tmp;
            gm.pieceManager.Play(play);

            // Puis s'il y avait une pièce qui avait été capturé, il faut la créer !
            if(data.plays[indicePlay - 1].hasCapture) {
                PieceData pd = data.plays[indicePlay - 1].pieceCaptured;
                gm.pieceManager.AddPieceFromPieceData(pd);
            }

            SetIndicePlay(indicePlay - 1);
        }
    }

    public void GotoEnd() {
        for (; indicePlay < data.plays.Count;)
            PlayNextPlay();
    }

    public void GotoStart() {
        for (; indicePlay > 0;)
            UndoLastPlay();
    }

    public PieceModelData GetModelFromData(PieceData pieceData) {
        foreach(PieceModelData pmd in data.whitePiecesModels) {
            if(pmd.data.Equals(pieceData)) {
                return pmd;
            }
        }
        foreach(PieceModelData pmd in data.blackPiecesModels) {
            if(pmd.data.Equals(pieceData)) {
                return pmd;
            }
        }
        throw new SystemException("On n'a pas réussi à trouver le PieceModelData correspondant à un PieceData !");
    }

    private void SetIndicePlay(int newIndicePlay) {
        indicePlay = newIndicePlay;
        textIndicePlay.text = indicePlay.ToString();
    }

    public static T Clone<T>(T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (System.Object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }
}
