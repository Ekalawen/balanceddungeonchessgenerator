﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LinearMoveData {
    [SerializeField]public bool canCapture = true;
    [SerializeField]public bool mustCapture = false;
    [SerializeField]public bool firstMoveOnly = false;
    [SerializeField]public int distance = int.MaxValue;
    [SerializeField]public List<Vector2Int> directions;
    [SerializeField]public bool circularMove = true;
    [NonSerialized] public PieceData pieceData;

    public LinearMoveData(bool canCapture,
                          bool mustCapture,
                          bool firstMoveOnly,
                          int distance,
                          List<Vector2Int> directions,
                          bool circularMove,
                          PieceData pieceData) {
        this.canCapture = canCapture;
        this.mustCapture = mustCapture;
        this.firstMoveOnly = firstMoveOnly;
        this.distance = distance;
        this.directions = directions;
        this.circularMove = circularMove;
        this.pieceData = pieceData;
    }

    public LinearMoveData(LinearMove lm) {
        this.canCapture = lm.canCapture;
        this.mustCapture = lm.mustCapture;
        this.firstMoveOnly = lm.firstMoveOnly;
        this.distance = lm.distance;
        this.directions = lm.directions;
        this.circularMove = lm.circularMove;
        this.pieceData = lm.piece.GetData();
    }

    public LinearMoveData(LinearMoveData other, PieceData associatedPieceData) {
        this.canCapture = other.canCapture;
        this.mustCapture = other.mustCapture;
        this.firstMoveOnly = other.firstMoveOnly;
        this.distance = other.distance;
        this.directions = other.directions;
        this.circularMove = other.circularMove;
        this.pieceData = associatedPieceData;
    }

    public override bool Equals(object obj) {
        LinearMoveData other = (LinearMoveData)obj;
        if (obj == null)
            return false;

        bool b1 = canCapture == other.canCapture;
        bool b2 = mustCapture == other.mustCapture;
        bool b3 = firstMoveOnly == other.firstMoveOnly;
        bool b4 = distance == other.distance;
        bool b5 = directions.Count == other.directions.Count;
        if (b5) {
            for (int i = 0; i < directions.Count; i++) {
                if(directions[i] != other.directions[i]) {
                    b5 = false;
                    break;
                }
            }
        }

        bool b6 = circularMove == other.circularMove;

        return b1 && b2 && b3 && b4 && b5 && b6;
    }

    public List<Vector2Int> GetPositions() {
        List<Vector2Int> positions = new List<Vector2Int>();

        if (firstMoveOnly && pieceData.hasMove)
            return positions;

        for(int i = 0; i < directions.Count; i++) {
            for(int j = 1; j <= distance; j++) {
                Vector2Int current = pieceData.position;
                current.x += directions[i].x * j;
                current.y += directions[i].y * j;
                if (GetBoard().IsIn(current)) {
                    positions.Add(current);
                }
            }
        }

        return positions;
    }

    public List<Vector2Int> GetPositionsAccessibles() {
        List<Vector2Int> positions = GetPositions();
        List<Vector2Int> positionsFinales = new List<Vector2Int>();

        foreach(Vector2Int position in positions) {
            if(IsAccessible(position)) {
                positionsFinales.Add(position);
            }
        }

        return positionsFinales;
    }

    public bool IsAccessible(Vector2Int destination, bool noConsiderMarked = false) {
        Vector2Int direction = GetDiretionTo(destination);
        if (direction == Vector2Int.zero)
            return false;

        Vector2Int currentPos = pieceData.position + direction;
        int currentDistance = 1;
        while(currentPos != destination && currentDistance <= distance && GetBoard().IsIn(currentPos)) {
            if(!GetBoard().IsWalkable(currentPos, noConsiderMarked)) {
                return false;
            }
            currentPos += direction;
            currentDistance++;
        }

        if (!GetBoard().IsIn(currentPos) || currentDistance > distance)
            return false;

        bool capturable = GetBoard().IsCapturable(currentPos, pieceData.color, noConsiderMarked);
        bool constraintsCanCapture = !(!canCapture && GetPieceManager().IsPiece(currentPos, noConsiderMarked));
        bool constraintsMustCapture = !(mustCapture && !GetPieceManager().IsPiece(currentPos, noConsiderMarked));

        return capturable && constraintsCanCapture && constraintsMustCapture;
    }

    protected Vector2Int GetDiretionTo(Vector2Int position) {
        Vector2Int diff = position - pieceData.position;
        foreach(Vector2Int direction in directions) {
            if(IsMultiple(diff, direction)) {
                return direction;
            }
        }
        //Debug.Log("Cette position n'est pas accessible à partir de nos mouvements !" + position + " " + piece.data.position);
        return Vector2Int.zero;
    }

    private bool IsMultiple(Vector2Int big, Vector2Int small) {
        // La bonne méthode :)
        if (big == small)
            return true;
        if (big == Vector2Int.zero || small == Vector2Int.zero)
            return false;
        Vector2 bigNormalized = new Vector2(big.x, big.y).normalized;
        Vector2 smallNormalized = new Vector2(small.x, small.y).normalized;
        return bigNormalized == smallNormalized;
    }


    private GameManager GetGM() {
        return pieceData.gm;
    }
    private Board GetBoard() {
        return pieceData.gm.board;
    }
    private PieceManager GetPieceManager() {
        return pieceData.gm.pieceManager;
    }

    public void AddCircularMove() {
        if(circularMove) {
            List<Vector2Int> newDirections = new List<Vector2Int>();
            foreach(Vector2Int direction in directions) {
                Vector2Int forward = direction;
                newDirections.Add(forward);
                forward = LinearMove.RotateRight(forward);
                newDirections.Add(forward);
                forward = LinearMove.RotateRight(forward);
                newDirections.Add(forward);
                forward = LinearMove.RotateRight(forward);
                newDirections.Add(forward);
            }
            directions = newDirections;
        }
    }
}
