﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCTSNode {

    private static int nextId = 0;

    public int id; // Permet d'identifier les MCTSNode, leurs id sont uniques !
    public int N = 0; // Nombre de parties
    public float W = 0; // Payoff/action-value total
    public float P = -1; // Prior Probability, initialisé à 0 pour avertir qu'elle n'est pas initialisé !
    public float Q = 1; // Q = mean payoff/action-value
    // Computed values :
    // T = numéro de la recherche (1er, 2em ...) (c'est le N du parent)

    public MCTSNode parent;
    public List<MCTSNode> children;
    public GameState gs;

    public MCTSNode(GameState gs, MCTSNode parent = null) {
        id = MCTSNode.nextId++;
        this.parent = parent;
        if (parent != null)
            parent.children.Add(this);
        this.children = new List<MCTSNode>();
        this.gs = gs;
    }

    public bool IsRoot() {
        return parent == null;
    }
    public bool IsLeaf() {
        return children.Count == 0;
    }

    // On peut choisir entre ces 2 fonctions
    public float GetPUCBValue() {
        // Ne pas avoir oublié d'avoir setté P ! ==> OK :)
        int T = parent.N;
        float C = (N > 0) ? Mathf.Sqrt(3 * Mathf.Log(T) / 2 * N) : 0;
        float M = (T > 0) ? 2 / P * Mathf.Sqrt(Mathf.Log(T) / T) : 2 / P;
        return Q + C - M;
    }
    public float GetUCB1Value() {
        int T = parent.N;
        float parametreDExploration = Mathf.Sqrt(2);
        float exploration = Mathf.Sqrt(Mathf.Log(T) / N + 1);
        return Q + parametreDExploration * exploration;
    }
    public float GetGreedyValue() {
        return Q;
    }

    List<int> indicesValidProbabilities = null;
    public List<int> GetIndiceValidProbabilities(GameManager gm) {
        if (indicesValidProbabilities == null) {
            indicesValidProbabilities = new List<int>();
            foreach (MCTSNode child in children) {
                Play lastPlay = child.gs.lastPlay;
                int indice = gm.GetIndiceOutputsFromPlay(lastPlay);
                indicesValidProbabilities.Add(indice);
            }
        }

        return indicesValidProbabilities;
    }

    public bool IsFinal(GameManager gm) {
        return gm.victoryManager.IsTimeOut(gs.nbMoves)
            || gm.victoryManager.IsLostForPiecesPositions(gs.pieces, gs.currentColor);
    }

    //// Q = mean payoff/action-value
    //public float Q() {
    //    return (N > 0) ? W / (float)N : 1.0f;
    //}

    //// T = numéro de la recherche (1er, 2em ...) (c'est le N du parent)
    //public int T() {
    //    if (parent == null)
    //        throw new System.Exception("Il faut trouver une valeur par défaut pour le root pour T !");
    //    return parent.N;
    //}

    public void UpdateNWQ(ChessResult result) {
        if((int)gs.currentColor == (int)result) {
            W++;
        } else if (result == ChessResult.TIED) {
            W += 0.5f;
        }
        N++;
        Q = (N > 0) ? W / (float)N : 1.0f;
    }

    public void ClearChildren() {
        children.Clear();
        indicesValidProbabilities = null;
    }
}
