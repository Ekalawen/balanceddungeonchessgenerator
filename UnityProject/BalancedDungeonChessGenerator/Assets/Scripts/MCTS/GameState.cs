﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Numpy;
//using Numpy.Models;
//using Keras;

public class GameState {

    public List<PieceData> pieces;
    private NDarrayF state;
    private int boardSize;
    private int nbWhitePieces;
    private int nbBlackPieces;
    private int nbTerrainType;
    private int N, M;
    public int nbMoves;
    public ChessColor currentColor;
    public Play lastPlay;
    private GameManager gm;

    public GameState(List<PieceData> pieces, int boardSize, int nbWhitePieces, int nbBlackPieces, int nbTerrainType, ChessColor currentColor, int nbMoves, GameManager gm) {
        this.pieces = pieces;
        this.boardSize = boardSize;
        this.nbWhitePieces = nbWhitePieces;
        this.nbBlackPieces = nbBlackPieces;
        this.nbTerrainType = nbTerrainType;
        this.nbMoves = nbMoves;
        this.currentColor = currentColor;
        this.gm = gm;
        N = boardSize;
        M = nbWhitePieces + nbBlackPieces + nbTerrainType + 2; // +2 pour la couleur et le nombre de moves !
        this.lastPlay = new Play(new Vector2Int( -1, -1 ), new Vector2Int( -1, -1)); // Just to crash

        state = new NDarrayF(0);
    }

    // Generate a GameState as a successor of an other
    public GameState GenerateSuccessor(List<PieceData> newPieces, Play lastPlay) {
        List<PieceData> piecesCopy = new List<PieceData>();
        foreach (PieceData piece in newPieces)
            piecesCopy.Add(new PieceData(piece));
        GameState gs = new GameState(piecesCopy,
            boardSize,
            nbWhitePieces,
            nbBlackPieces,
            nbTerrainType,
            gm.OtherColor(currentColor),
            nbMoves + 1,
            gm);
        gs.lastPlay = lastPlay;

        return gs;
    }

    public NDarrayF GetState() {
        if (state.GetNbElements() == 0) {
            GenerateState();
        }
        return state;
    }

    public NDarrayF GetStateCopy() {
        return new NDarrayF(GetState());
    }

    // Generate a GameState as a successor of an other but without copying pieces (gain time)
    public GameState GenerateSuccessorWithoutPieces(Play lastPlay) {
        GameState gs = new GameState(new List<PieceData>(),
            boardSize,
            nbWhitePieces,
            nbBlackPieces,
            nbTerrainType,
            gm.OtherColor(currentColor),
            nbMoves + 1,
            gm);
        gs.lastPlay = lastPlay;

        // But we still fill the state as a copy
        gs.state = new NDarrayF(this.GetState());

        // We need to ajust color and nbMoves too !
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                gs.state[i, j, M - 2] = (gm.OtherColor(currentColor) == ChessColor.BLACK) ? 0.0f : 1.0f;
                gs.state[i, j, M - 1] = nbMoves + 1;
            }
        }

        // And then we want to play the lastPlay
        gs.PlayValidPlayInState(lastPlay);

        return gs;
    }

    // THE PLAY MUST BE VALID !!!
    public void PlayValidPlayInState(Play play) {
        // 1) Get the plane of the piece
        int indicePlanePiece = -1;
        for(int i = 0; i < GetNbTypesPieces(); i++) {
            if(state[play.from.x, play.from.y, i] == 1.0f) {
                if (indicePlanePiece != -1)
                    throw new System.Exception("2 pièces ne devrait pas se trouver au même endroit !");
                indicePlanePiece = i;
            }
        }
        if (indicePlanePiece == -1) {
            throw new System.Exception("Nous n'avons pas trouvé de pièce à cet emplacement !\n" +
                "Emplacement = " + play.from + "\n" + this);
        }

        // 2) Remove the piece from it's original position
        state[play.from.x, play.from.y, indicePlanePiece] = 0.0f;

        // 3) Remove all pieces from it's destination position
        for(int i = 0; i < GetNbTypesPieces(); i++) {
            state[play.to.x, play.to.y, i] = 0.0f;
        }

        // 4) If destination isn't a castel, put the piece to it's destination position
        if(state[play.to.x, play.to.y, GetIndicePlateau(TileType.DUNGEON)] != 1.0f) {
            state[play.to.x, play.to.y, indicePlanePiece] = 1.0f;
        }
    }

    private void GenerateState() {
        // On crée le super array ! <3
        // Initialisé avec uniquement des 0
        state = new NDarrayF(N, N, M);

        // Puis on va le remplir petit à petit !
        // Pour les pièces
        foreach (PieceData piece in pieces) {
            AddPiece(piece);
        }
        // Pour les cases
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                AddTile(i, j, gm.board.map[i][j].type);
            }
        }
        // Pour la couleur et le tour en cours
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                state[i, j, M - 2] = (currentColor == ChessColor.BLACK) ? 0.0f : 1.0f;
                state[i, j, M - 1] = nbMoves;
            }
        }
    }

    private void AddPiece(PieceData piece) {
        int indicePlateau = GetIndicePlateau(piece);
        int i = piece.position.x;
        int j = piece.position.y;
        state[i, j, indicePlateau] = 1.0f;
    }

    private void AddTile(int i, int j, TileType type) {
        int indicePlateau = GetIndicePlateau(type);
        state[i, j, indicePlateau] = 1.0f;
    }

    private int GetIndicePlateau(PieceData piece) {
        // On met les blanches en premières, puis ensuite les noires
        List<Piece> whitePieces = gm.pieceManager.whitePiecesModels;
        List<Piece> blackPieces = gm.pieceManager.blackPiecesModels;
        for(int i = 0; i < nbWhitePieces; i++) {
            if(piece.Equals(whitePieces[i].GetData())) {
                return i;
            }
        }
        for(int i = 0; i < nbBlackPieces; i++) {
            if(piece.Equals(blackPieces[i].GetData())) {
                return nbWhitePieces + i;
            }
        }

        throw new System.Exception("On a pas réussi à trouver l'indice d'une pièce !");
    }

    private int GetIndicePlateau(TileType tileType) {
        return nbWhitePieces + nbBlackPieces + (int)tileType; 
    }

    public int GetNbTypesPieces() {
        return nbWhitePieces + nbBlackPieces;
    }

    public List<PieceData> GetPiecesCopy() {
        List<PieceData> copy = new List<PieceData>();
        foreach (PieceData piece in pieces)
            copy.Add(new PieceData(piece));
        return copy;
    }

    // N'a de sens que si le noeud est final !
    public ChessResult GetWinner() {
        // Si il ne reste plus de temps ... !
        if (nbMoves >= gm.nbMaxTurns)
            return ChessResult.TIED;
        // La simulation s'arrête lorsqu'un joueur a perdu, donc le gagnant c'est l'autre =)
        return (ChessResult)gm.OtherColor(currentColor);
    }

    public string GetStringOfPlanes() {
        string str = "";

        for(int i = 0; i < nbWhitePieces; i++) {
            PieceData piece = gm.pieceManager.GetPieceDataFromIndice(i);
            str += "W_" + piece.GetType().ToString() + " ";
        }
        for(int i = 0; i < nbBlackPieces; i++) {
            PieceData piece = gm.pieceManager.GetPieceDataFromIndice(i + nbWhitePieces);
            str += "B_" + piece.GetType().ToString() + " ";
        }
        for (int i = 0; i < nbTerrainType; i++) {
            str += ((TileType)i).ToString() + " ";
        }
        str += "COLOR ";
        str += "NB_MOVES";

        return str;
    }

    public override string ToString() {
        return "state = " + GetStringOfPlanes() + "\n" + state;
    }
}
