﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Numpy;


public class MCTS {

    // Prend un état du jeu et renvoie les probabilités de chaque coup !
    public delegate NDarrayF FonctionDEvaluation(GameState gs);

    private MCTSNode root;
    private FonctionDEvaluation fonctionDEvaluation;
    private GameManager gm;
    private MCTSNode bestNode;

    public MCTS(GameState initialState, FonctionDEvaluation fonctionDEvaluation, GameManager gm) {
        this.gm = gm;

        // Le root devient la configuration initiale
        root = new MCTSNode(initialState, null);

        // On retient la fonction d'évaluation
        this.fonctionDEvaluation = fonctionDEvaluation;
    }

    public void Run(int nbSimulations) {
        // Tant que les contraintes ne sont pas satisfaites :
        float dureeSelected = 0;
        float dureeExpansion = 0;
        float dureeSimulation = 0;
        float dureeBackpropagation = 0;
    dureeComputeP = 0;
    dureeFindMaxPUCB = 0;
    dureeNN = 0;
    dureeGetIndiceValidProbabilities = 0;
        for (int i = 0; i < nbSimulations; i++) {
            float debut = Time.realtimeSinceStartup;
            //Debug.Log("TOUR " + (i + 1));

            // Sélection
            MCTSNode selectedNode = Selection();
            float finSelected = Time.realtimeSinceStartup;
            dureeSelected += (finSelected - debut);
            //gm.LogTime("Durée Selection = " + (finSelected - debut) + "s");

            // Expansion
            MCTSNode leafNode = Expansion(selectedNode);
            float finExpansion = Time.realtimeSinceStartup;
            dureeExpansion += (finExpansion - finSelected);
            //gm.LogTime("Durée Expansion = " + (finExpansion - finSelected) + "s");

            // Simulation
            MCTSNode finalNode = (leafNode != null) ? Simulation(leafNode) : selectedNode;
            float finSimulation = Time.realtimeSinceStartup;
            dureeSimulation += (finSimulation - finExpansion);
            //gm.LogTime("Durée Simulation = " + (finSimulation - finExpansion) + "s");

            // Retropropagation
            Retropropagation(leafNode, finalNode.gs.GetWinner());
            float fin = Time.realtimeSinceStartup;
            dureeBackpropagation += (fin - finSimulation);
            //gm.LogTime("Durée Retropropagation = " + (fin - finSimulation) + "s");
            //gm.LogTime("Durée SIMULATION = " + (fin - debut) + "s");
        }
        gm.LogTime("Durée Selected = " + dureeSelected + "s\n" +
            "Durée ComputeP = " + dureeComputeP + "s\n" +
            "Durée ComputeP/NN = " + dureeNN + "s\n" +
            "Durée ComputeP/GetIndice = " + dureeGetIndiceValidProbabilities + "s\n" +
            "Durée FindMaxPUCB = " + dureeFindMaxPUCB + "s");
        gm.LogTime("Durée Expansion = " + dureeExpansion + "s");
        gm.LogTime("Durée Simulation = " + dureeSimulation + "s");
        gm.LogTime("Durée Backpropagation = " + dureeBackpropagation + "s");

        // Selectionner le meilleur noeud
        bestNode = GetBestNode();

        // Stocker toutes les informations utiles :
            // Le winrate à partir de cette position
            // Les probabilités de chaque coups
            // Le meilleur coup à jouer
        // Ne retourne rien, il faudra appeller les bons getteurs pour avoir les résultats !
    }

    float dureeComputeP = 0;
    float dureeFindMaxPUCB = 0;
    private MCTSNode Selection() {
        // Parcourir l'arbre depuis la racine en fonction des paramètres stockés dans les noeuds
        // Pour arriver jusqu'à une feuille
        MCTSNode currentNode = root;
        while(!currentNode.IsLeaf()) {
            float debut = Time.realtimeSinceStartup;
            // Calculer P (la probabilité à priori) pour tous les enfants de notre noeud
            ComputePForAllChildren(currentNode);
            float finComputeP = Time.realtimeSinceStartup;
            dureeComputeP += (finComputeP - debut);

            // Prendre l'enfant le plus prometteur selon les critères d'exploration et d'exploitation
            // 2 algorithmes
            // PUCB qui prends en compte l'appriori du NN
            // UCB1 qui ne le prends pas en compte, mais plus simple à calculer
            List<MCTSNode> children = currentNode.children;
            int indBestValue = 0;
            float bestValue = children[0].GetPUCBValue();
            for(int i = 1; i < children.Count; i++) {
                float value = children[i].GetPUCBValue();
                if(value > bestValue) {
                    indBestValue = i;
                    bestValue = value;
                }
            }
            currentNode = children[indBestValue];

            float finFindMaxPUCB = Time.realtimeSinceStartup;
            dureeFindMaxPUCB += (finFindMaxPUCB - finComputeP);
            //gm.LogTime("duréeComputeP = " + dureeComputeP);
            //gm.LogTime("duréeFindMaxPUCB = " + dureeFindMaxPUCB);
        }

        return currentNode;
    }

    float dureeNN = 0;
    float dureeGetIndiceValidProbabilities = 0;
    private void ComputePForAllChildren(MCTSNode node) {
        if (node.children.Count == 0)
            return;
        if (node.children[0].P != -1.0f) // Les P ont déjà été calculés pour ce noeud !
            return;

        float debut = Time.realtimeSinceStartup;
        NDarrayF probabilities = fonctionDEvaluation(node.gs);
        float finNN = Time.realtimeSinceStartup;
        dureeNN += (finNN - debut);

        NDarray<int> indiceValidProbabilities = new NDarray<int>(node.GetIndiceValidProbabilities(gm));
        NDarrayF validProbabilities = probabilities[indiceValidProbabilities];
        if (validProbabilities.GetNbElements() != node.children.Count)
            throw new System.Exception("Erreur sournoise à régler !");
        for (int i = 0; i < node.children.Count; i++) {
            node.children[i].P = (float)validProbabilities[i];
        }

        float finGetIndiceValidProbabilities = Time.realtimeSinceStartup;
        dureeGetIndiceValidProbabilities += (finGetIndiceValidProbabilities - finNN);
    }

    private MCTSNode Expansion(MCTSNode node) {
        // Trouver tous les coups possibles à partir de ce GameState
        List<PieceData> pieces = node.gs.GetPiecesCopy();
        List<Play> possiblesPlays = gm.pieceManager.GetAllPlaysFromPiecesPositions(pieces, node.gs.currentColor);

        // Pour chacun
        foreach (Play play in possiblesPlays) {
            // Copier les datas pour éviter de les modifiers
            List<PieceData> piecesCopy = new List<PieceData>();
            foreach(PieceData piece in pieces) { piecesCopy.Add(new PieceData(piece)); }
            // Créer un nouveau MCTSNode et le relier à notre node
            List<PieceData> newPieces = gm.pieceManager.GetPositionsAfterPlay(piecesCopy, play);
            GameState gs = node.gs.GenerateSuccessor(newPieces, play);
            MCTSNode child = new MCTSNode(gs, parent: node);
        }

        // Choisir l'un des enfants en utilisant la fonction d'évaluation du NN !
        // ATTENTION C'EST BEAUCOUP PLUS COMPLIQUE QUE CELA
        // CAR LES PROBABILITES SONT LES PROBAS DE tous LES COUPS
        // ET PAS JUSTE LES COUPS POSSIBLES !!!
        // Il faudra donc retenir uniquement les probas des coups possibles avant :)
        NDarrayF probabilities = fonctionDEvaluation(node.gs); // Il faudra peut-être retenir cela !
        NDarray<int> indiceValidProbabilities = new NDarray<int>(node.GetIndiceValidProbabilities(gm));
        NDarrayF validProbabilities = probabilities[indiceValidProbabilities];

        // On va retenir ces probabilitées, elles seront utiles par la suite pour la sélection !
        for(int i = 0; i < validProbabilities.GetNbElements(); i++) {
            node.children[i].P = validProbabilities[i];
        }

        int argmax = NDarrayF.Argmax(validProbabilities);
        MCTSNode chosenNode = (argmax != -1) ? node.children[argmax] : null;

        // Et le retourner
        return chosenNode;
    }

    private MCTSNode Simulation(MCTSNode node) {
        MCTSNode currentNode = node;
        List<MCTSNode> childrenNodeSave = node.children;

        // Tant que node n'est pas un GameState final
        while (!currentNode.IsFinal(gm)) {
            // Jouer un coup en utilisant le NN
            MCTSNode nextNode = GetChosenNodeByFonctionEvaluation(currentNode);
            if (nextNode == null)
                break;
            currentNode = nextNode;
        }

        // On a détruit les children de node donc on les lui remet !
        node.children = childrenNodeSave;

        // Retourner le gagnant
        return currentNode;
    }

    private MCTSNode GetChosenNodeByFonctionEvaluation(MCTSNode node) {
        //// Trouver tous les coups possibles à partir de ce GameState
        //List<PieceData> pieces = node.gs.GetPieces();
        //List<Play> possiblesPlays = gm.pieceManager.GetAllPlaysFromPiecesPositions(pieces, node.gs.GetCurrentColor());

        //// Pour chacun
        //foreach (Play play in possiblesPlays) {
        //    // Créer un nouveau MCTSNode et le relier à notre node
        //    // Ce MCTSNode ne content pas la liste des pièces, on la rajoutera après pour économiser des calculs
        //    GameState gs = node.gs.GenerateSuccessorWithoutPieces(play);
        //    MCTSNode child = new MCTSNode(gs, parent: node);
        //}

        //// Choisir l'un des enfants en utilisant la fonction d'évaluation du NN !
        //NDarrayF probabilities = fonctionDEvaluation(node.gs);
        //NDarray<int> indiceValidProbabilities = new NDarray<int>(node.GetIndiceValidProbabilities(gm));
        //NDarrayF validProbabilities = probabilities[indiceValidProbabilities];
        //int argmax = NDarrayF.Argmax(validProbabilities);
        //MCTSNode chosenNode = (argmax != -1) ? node.children[argmax] : null;

        //// On rajoutes les pièces juste au node choisi
        //List<PieceData> newPieces = gm.pieceManager.GetPositionsAfterPlay(pieces, chosenNode.gs.GetLastPlay());
        //chosenNode.gs.SetPieces(newPieces);

        //// Mais il faut enlever les enfants qui ont étés générés de node
        //node.ClearChildren();

        //// Et le retourner
        //return chosenNode;

        /// Je ne sais pas laquelle de ces 2 versions est la plus rapide ! x)
        /// Il faudra faire de meilleurs tests sur des données plus importantes pour trancher.
        /// Mais je ne pense pas que ce soit de là que vienne le problème pour le moment !

        MCTSNode chosenNode = Expansion(node);
        // Mais il faut enlever les enfants qui ont étés générés de node
        node.ClearChildren();
        return chosenNode;
    }

    private void Retropropagation(MCTSNode node, ChessResult result) {
        // Tant que le node n'est pas null
        while (node != null) {
            // Updater le node en fonction du résultat
            node.UpdateNWQ(result);

            // node <== node.parent
            node = node.parent;
        }
    }

    private MCTSNode GetBestNode() {
        // Comparer tous les enfants du root et renvoyer celui ayant le meilleur ratio !
        // On utilise ici une stratégie greedy
        List<MCTSNode> children = root.children;
        children = children.OrderByDescending(x => x.GetGreedyValue()).ToList();
        MCTSNode chosenNode = children[0];
        return chosenNode;
    }

    private Play GetPlayFrom2States(/*GS start et GS end*/) {
        return new Play();
    }

    public NDarrayF GetExpectedOuput() {
        NDarrayF output = new NDarrayF(gm.GetNbOutputs());
        float sum = 0;
        float D = 0; // D is for prevent big exponentials to cause float overflow
        for (int i = 0; i < root.children.Count; i++)
            D = Mathf.Min(D, -root.children[i].Q);

        float maxWinrate = root.children[0].Q;
        foreach (MCTSNode child in root.children) {
            float value = child.Q;
            if(value > maxWinrate) {
                maxWinrate = value;
            }
            sum += Mathf.Exp(value + D);
        }
        foreach (MCTSNode child in root.children) {
            float value = Mathf.Exp(child.Q + D) / sum;
            int indice = gm.GetIndiceOutputsFromPlay(child.gs.lastPlay);
            output[indice] = value;
        }

        // Rajouter le dernier output qui correspond au winrate attendu ! :)
        output[output.GetNbElements() - 1] = maxWinrate;

        return output;
    }

    public Play GetBestPlay() {
        return bestNode.gs.lastPlay;
    }
}
