﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PieceType { PAWN, BISHOP, KNIGHT, TOWER, QUEEN, KING };
public enum ChessColor { WHITE, BLACK };
public enum ChessResult { WHITE_WIN, BLACK_WIN, TIED };

[Serializable]
public class PieceData {

    [SerializeField]public PieceType type;
    [SerializeField]public ChessColor color;
    [SerializeField]public List<LinearMoveData> moves;
    [SerializeField]public Vector2Int position;
    [SerializeField]public bool hasMove = false;
    [SerializeField]public Piece pieceVue = null;
    [SerializeField]public bool markCaptured = false;
    [NonSerialized] public GameManager gm;

    public PieceData(PieceType type,
                     ChessColor color,
                     List<LinearMoveData> moves,
                     Vector2Int position,
                     GameManager gm,
                     Piece pieceVue = null) {
        this.type = type;
        this.color = color;
        this.moves = moves;
        this.position = position;
        this.gm = gm;
        this.pieceVue = pieceVue;
    }
    public PieceData(PieceType type,
                     ChessColor color,
                     List<LinearMove> moves,
                     Vector2Int position,
                     GameManager gm,
                     Piece pieceVue = null) {
        this.type = type;
        this.color = color;
        this.position = position;
        this.gm = gm;
        this.pieceVue = pieceVue;
        this.moves = new List<LinearMoveData>();
        foreach(LinearMove lm in moves) {
            LinearMoveData lmd = new LinearMoveData(lm);
            lmd.pieceData = this;
            this.moves.Add(lmd);
        }
    }

    public PieceData(PieceData other) {
        this.type = other.type;
        this.color = other.color;
        this.position = other.position;
        this.hasMove = other.hasMove;
        this.pieceVue = other.pieceVue;
        this.markCaptured = other.markCaptured;
        this.gm = other.gm;
        this.moves = new List<LinearMoveData>();
        foreach(LinearMoveData lmd in other.moves) {
            LinearMoveData newLmd = new LinearMoveData(lmd, associatedPieceData: this);
            this.moves.Add(newLmd);
        }
    }

    public override bool Equals(object obj) {
        PieceData other = (PieceData)obj;
        if (other == null)
            return false;

        // Tous les moves doivent être egaux !
        bool equalMoves = other.moves.Count == moves.Count;
        if(equalMoves) {
            for(int i = 0; i < moves.Count; i++) {
                if (!moves[i].Equals(other.moves[i])) {
                    equalMoves = false;
                    break;
                }
            }
        }

        return type == other.type && color == other.color && equalMoves;
    }

    public PieceData GetCopy() {
        PieceData newData = new PieceData(type, color, moves, position, null);
        newData.hasMove = hasMove;
        return newData;
    }

    public void SetMoves(List<LinearMove> newMoves) {
        this.moves = new List<LinearMoveData>();
        foreach(LinearMove lm in newMoves) {
            this.moves.Add(lm.data);
        }
    }

    public bool CanGoTo(Vector2Int position, bool noConsiderMarked = false) {
        foreach(LinearMoveData move in moves) {
            if (move.IsAccessible(position, noConsiderMarked))
                return true;
        }
        return false;
    }

    public List<Vector2Int> GetDestinations() {
        List<Vector2Int> pos = new List<Vector2Int>();
        foreach(LinearMoveData move in moves) {
            pos.AddRange(move.GetPositions());
        }
        return pos;
    }

    public List<Vector2Int> GetDestinationsAccessibles() {
        List<Vector2Int> pos = new List<Vector2Int>();
        foreach (LinearMoveData move in moves) {
            pos.AddRange(move.GetPositionsAccessibles());
        }
        return pos;
    }

    public List<Vector2Int> GetAllValidMoves() {
        List<Vector2Int> pos = new List<Vector2Int>();
        foreach (Vector2Int dest in GetDestinationsAccessibles()) {
            Play play = new Play(position, dest);
            if (gm.pieceManager.IsKingSafe(play)) {
                pos.Add(dest);
            }
        }
        return pos;
    }

    public bool HasValidMove() {
        return GetAllValidMoves().Count > 0;
    }

    public void MoveTo(Vector2Int dest) {
        // On ne fait pas de vérifications ici car on pourrait déplacer une pièce sans que ça passe par ses propres mouvements !
        position = dest;
        hasMove = true;
    }

    public void CaptureFrom(List<PieceData> pieces) {
        //gm.pieceManager.pieces.Remove(this);
        // pieces.Remove(this);
        for(int i = 0; i < pieces.Count; i++) {
            PieceData pieceData = pieces[i];
            if(pieceData.Equals(this) && pieceData.position == this.position) {
                pieces.RemoveAt(i);
                break;
            }
        }
    }
}
