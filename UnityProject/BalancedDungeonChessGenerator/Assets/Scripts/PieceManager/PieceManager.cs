﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceManager : MonoBehaviour {

    public int nbPieces = 5;
    public List<Piece> whitePiecesModels;
    public List<Piece> blackPiecesModels;

    private Board board;
    protected List<Piece> piecesAtInit;
    public List<PieceData> piecesDatas;
    private GameManager gm;

    public void Initialize() {
        board = FindObjectOfType<GameManager>().board;
        gm = FindObjectOfType<GameManager>();

        foreach(Piece p in whitePiecesModels) {
            // On ne les initialises pas car ce ne sont que des models !
            p.Initialize(new Vector2Int(-1, -1), disableInstancing: true);
        }
        foreach(Piece p in blackPiecesModels) {
            // On ne les initialises pas car ce ne sont que des models !
            p.Initialize(new Vector2Int(-1, -1), disableInstancing: true);
        }
    }

    public void LoadFromHistory() {
        whitePiecesModels = new List<Piece>();
        blackPiecesModels = new List<Piece>();
        foreach(PieceModelData pmd in gm.history.data.whitePiecesModels) {
            Piece p = gameObject.AddComponent<Piece>();
            p.SetData(pmd.data);
            p.modelToInstantiate = pmd.model;
            whitePiecesModels.Add(p);
        }
        foreach(PieceModelData pmd in gm.history.data.blackPiecesModels) {
            Piece p = gameObject.AddComponent<Piece>();
            p.SetData(pmd.data);
            p.modelToInstantiate = pmd.model;
            blackPiecesModels.Add(p);
        }

        piecesAtInit = new List<Piece>(); 
        foreach(PieceData pd in gm.history.data.pieces) {
            AddPieceFromPieceData(pd);
        }
        GeneratePiecesDataFromPiecesAtInit();
    }

    protected void GeneratePiecesDataFromPiecesAtInit() {
        piecesDatas = new List<PieceData>();
        foreach(Piece piece in piecesAtInit) {
            piecesDatas.Add(piece.GetData());
        }
    }

    public void AddPieceFromPieceData(PieceData pd) {
        // Trouver le modèle correspondant
        PieceModelData pmd = gm.history.GetModelFromData(pd);

        // Instancier ce modèle
        GameObject go = new GameObject();
        go.name = "Piece";

        // Lui rajouter ensuite les composants nécessaires !
        Piece p = go.AddComponent<Piece>();
        piecesAtInit.Add(p);

        // Initialiser la pièce à partir des datas !
        p.InitializeFromData(pd, pmd.model);
        go.name = p.GetName();

        // L'ajouter aux piecesDatas (je suis un super commentaire :))
        if(piecesDatas != null)
            piecesDatas.Add(p.GetData());
    }

    public virtual void GeneratePieces() {
        piecesAtInit = new List<Piece>();
        for(int i = 0; i < nbPieces; i++) {
            int idPiece = Random.Range(0, whitePiecesModels.Count);
            GameObject go = Instantiate(whitePiecesModels[idPiece].gameObject);
            piecesAtInit.Add(go.GetComponent<Piece>());

            idPiece = Random.Range(0, blackPiecesModels.Count);
            go = Instantiate(blackPiecesModels[idPiece].gameObject);
            piecesAtInit.Add(go.GetComponent<Piece>());
        }

        PlacePiecesRandomly();
        GeneratePiecesDataFromPiecesAtInit();
    }

    private void PlacePiecesRandomly() {
        List<Vector2Int> possiblePositions = board.AllStayablePositions();
        for(int i = 0; i < piecesAtInit.Count; i++) {
            Vector2Int pos = possiblePositions[Random.Range(0, possiblePositions.Count)];
            piecesAtInit[i].Initialize(pos);
            possiblePositions.Remove(pos);
        }
    }

    public bool IsPiece(Vector2Int position, bool noConsiderMarked = false) {
        foreach(PieceData piece in piecesDatas) {
            if (piece != null) {
                if (piece.position == position) {
                    if (!noConsiderMarked || (noConsiderMarked && !piece.markCaptured))
                        return true;
                }
            }
        }
        return false;
    }

    public PieceData GetPiece(Vector2Int position, bool noConsiderMarked = false) {
        foreach(PieceData piece in piecesDatas) {
            if (piece.position == position) {
                if (!noConsiderMarked || (noConsiderMarked && !piece.markCaptured)) {
                    return piece;
                }
            }
        }
        return null;
    }

    public void Play(Play play, bool disableAnimation = false) {
        // Récupérer la pièce
        PieceData pieceData = GetPiece(play.from);
        if (pieceData == null)
            throw new System.Exception("Le play n'est pas valide car on ne trouve pas de pièce à déplacer !");

        // Capturer une pièce s'il y a lieu
        if(IsPiece(play.to)) {
            PieceData autrePieceData = GetPiece(play.to);
            if(autrePieceData.color != pieceData.color) {
                autrePieceData.CaptureFrom(piecesDatas);
                autrePieceData.pieceVue.CaptureGameObject(disableAnimation);
                // Si c'était un dongeon on est aussi capturé !
                if(gm.board.map[play.to.x][play.to.y].type == TileType.DUNGEON) {
                    pieceData.CaptureFrom(piecesDatas);
                    if (gm.display) {
                        StartCoroutine(CapturePieceIn(pieceData, Piece.timeDeplacement, disableAnimation));
                    } else {
                        pieceData.pieceVue.CaptureGameObject(disableAnimation);
                    }
                }
            } else {
                throw new System.Exception("On essaye de capturer une pièce de notre propre couleur !");
            }
        }

        // Déplacer la pièce
        pieceData.MoveTo(play.to);
        StartCoroutine(pieceData.pieceVue.MoveGameObject(play.to, disableAnimation));
    }

    public void PlayInModel(Play play, PieceData piece) {
        // Récupérer la pièce
        // Piece piece = GetPiece(play.from); // Ne fonctionne pas car il peut y avoir plusieurs pièces sur la même case !
        if (piece == null)
            throw new System.Exception("Le play n'est pas valide car on ne trouve pas de pièce à déplacer !");
        if(piece.markCaptured)
            throw new System.Exception("Tentative de faire un play dans le modèle avec une pièce marqué comme capturé !");

        // Capturer une pièce s'il y a lieu
        if(IsPiece(play.to)) {
            PieceData autrePiece = GetPiece(play.to);
            if(autrePiece.color != piece.color) {
                autrePiece.markCaptured = true;
                if(gm.board.map[play.to.x][play.to.y].type == TileType.DUNGEON) { // Si c'était un dongeon on est aussi capturé !
                    piece.markCaptured = true;
                }
            } else {
                throw new System.Exception("On essaye de capturer une pièce de notre propre couleur !");
            }
        }

        // Déplacer la pièce
        piece.MoveTo(play.to);
    }

    public void UndoPlay(HistoryPlay play, bool disableAnimation = false) {
        // Faire le play dans l'autre sens
        Play reverse = new Play(play.play.to, play.play.from);
        Play(reverse, disableAnimation);

        // Puis s'il y avait une pièce qui avait été capturé, il faut la créer !
        if(play.hasCapture) {
            PieceData pd = play.pieceCaptured;
            gm.pieceManager.AddPieceFromPieceData(pd);
        }
    }

    public void UndoPlayInModel(HistoryPlay play, PieceData piece, bool hasMove) {
        //// S'il y avait une pièce qui avait été marké comme capturé, il faut la démarquer !
        foreach (PieceData p in piecesDatas)
            p.markCaptured = false;

        // Puis faire le play dans l'autre sens
        Play reverse = new Play(play.play.to, play.play.from);
        PlayInModel(reverse, piece); // Si c'est vraiment un reverse alors la pièce revient à sa position de départ et ne peut donc capturer personne !
        piece.hasMove = hasMove;

        //// S'il y avait une pièce qui avait été marké comme capturé, il faut la démarquer !
        foreach (PieceData p in piecesDatas)
            p.markCaptured = false;
    }

    private IEnumerator CapturePieceIn(PieceData piece, float seconds, bool disableAnimation = false) {
        if(!disableAnimation)
            yield return new WaitForSeconds(seconds);
        piece.pieceVue.CaptureGameObject(disableAnimation);
    }

    public List<PieceData> GetAllPieceOfTypeAndColor(PieceType type, ChessColor color) {
        List<PieceData> res = new List<PieceData>();
        foreach(PieceData p in piecesDatas) {
            if (p.color == color && p.type == type)
                res.Add(p);
        }
        return res;
    }
    public List<PieceData> GetAllPieceOfType(PieceType type) {
        List<PieceData> res = new List<PieceData>();
        foreach(PieceData p in piecesDatas) {
            if (p.type == type)
                res.Add(p);
        }
        return res;
    }
    public List<PieceData> GetAllPieceOfColor(ChessColor color) {
        List<PieceData> res = new List<PieceData>();
        foreach(PieceData p in piecesDatas) {
            if (p.color == color)
                res.Add(p);
        }
        return res;
    }

    public void Clean() {
        foreach (PieceData piece in piecesDatas) {
            piece.pieceVue.AutoDestroy();
        }
        piecesDatas.Clear();
        piecesAtInit.Clear();
    }

    public bool IsKingSafe(Play play) {
        // Faire le play mais sans les animations
        HistoryPlay historyPlay = new HistoryPlay(play, gm); // On retient l'HistoryPlay pour pouvoir annuler le play !
        PieceData movingPiece = GetPiece(play.from);
        bool hasMove = movingPiece.hasMove;
        PlayInModel(play, movingPiece);

        // Récupérer tous les rois du joueur qui vient de bouger
        ChessColor kingColor = movingPiece.color;
        ChessColor ennemyColor = movingPiece.color == ChessColor.WHITE ? ChessColor.BLACK : ChessColor.WHITE;
        List<PieceData> kings = GetAllPieceOfTypeAndColor(PieceType.KING, kingColor);
        List<PieceData> ennemies = GetAllPieceOfColor(ennemyColor);

        // Vérifier si un roi est en danger
        bool allSafe = true;
        foreach(PieceData king in kings) {
            foreach(PieceData ennemy in ennemies) {
                if(!ennemy.markCaptured) {
                    if(ennemy.CanGoTo(king.position, noConsiderMarked: true)) {
                        allSafe = false;
                        break;
                    }
                }
            }
            if (!allSafe)
                break;
        }

        // Défaire le play toujours sans les animations
        UndoPlayInModel(historyPlay, movingPiece, hasMove);

        return allSafe;
    }

    public List<PieceData> GetPiecesDatasCopy() {
        List<PieceData> newPieces = new List<PieceData>();
        for(int i = 0; i < piecesDatas.Count; i++) {
            PieceData piece = new PieceData(piecesDatas[i]);
            newPieces.Add(piece);
        }
        return newPieces;
    }

    public List<Play> GetAllPlaysFromPiecesPositions(List<PieceData> newPieces, ChessColor currentColor) {
        // On sauvegarde les pieces courrantes
        // Puis on travaille avec les pièces que l'on nous donne ! <3
        List<PieceData> savePieces = piecesDatas;
        piecesDatas = newPieces;

        // Générer la liste des plays
        List<Play> plays = new List<Play>();
        foreach(PieceData piece in piecesDatas) {
            if (piece.color == currentColor) {
                List<Vector2Int> destinations = piece.GetAllValidMoves();
                foreach (Vector2Int destination in destinations) {
                    Play play = new Play(piece.position, destination);
                    plays.Add(play);
                }
            }
        }

        // Remettre les plays sauvegardés
        piecesDatas = savePieces;

        return plays;
    }

    public List<PieceData> GetPositionsAfterPlay(List<PieceData> newPieces, Play play) {
        // On sauvegarde les pieces courrantes
        // Puis on travaille avec les pièces que l'on nous donne ! <3
        List<PieceData> savePieces = piecesDatas;
        piecesDatas = newPieces;

        // Trouver la position suivante
        Play(play, disableAnimation: true);
        List<PieceData> piecesAfterPlay = GetPiecesDatasCopy();

        // Remettre les plays sauvegardés
        piecesDatas = savePieces;

        return piecesAfterPlay;
    }

    public PieceData GetPieceDataFromIndice(int indice) {
        if(indice < whitePiecesModels.Count) {
            return whitePiecesModels[indice].GetData();
        } else if (indice - whitePiecesModels.Count < blackPiecesModels.Count) {
            return blackPiecesModels[indice - whitePiecesModels.Count].GetData();
        }
        return null;
    }
}
