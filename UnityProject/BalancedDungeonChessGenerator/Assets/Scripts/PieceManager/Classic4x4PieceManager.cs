﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic4x4PieceManager : PieceManager {

    public override void GeneratePieces() {
        piecesAtInit = new List<Piece>();
        GameObject go;
        Piece piece;

        // White
        // Cavalier
        go = Instantiate(whitePiecesModels[0].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 3));
        piecesAtInit.Add(piece);
        // Roi
        go = Instantiate(whitePiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 2));
        piecesAtInit.Add(piece);
        // Fou
        go = Instantiate(whitePiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 1));
        piecesAtInit.Add(piece);
        // Tour
        go = Instantiate(whitePiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 0));
        piecesAtInit.Add(piece);

        // Black
        // Cavalier
        go = Instantiate(blackPiecesModels[0].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(3, 0));
        piecesAtInit.Add(piece);
        // Roi
        go = Instantiate(blackPiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(3, 1));
        piecesAtInit.Add(piece);
        // Fou
        go = Instantiate(blackPiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(3, 2));
        piecesAtInit.Add(piece);
        // Tour
        go = Instantiate(blackPiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(3, 3));
        piecesAtInit.Add(piece);

        GeneratePiecesDataFromPiecesAtInit();
    }
}
