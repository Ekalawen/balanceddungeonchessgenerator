﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct PieceModelData {
    public PieceData data;
    public GameObject model;

    public PieceModelData(PieceData data, GameObject model) {
        this.data = data;
        this.model = model;
    }
}

public class Piece : MonoBehaviour {

    public static float timeDeplacement = 0.5f;
    public static float timeDeath = 2.0f;
    public static float speedDeath = 5.0f;

    public PieceType type;
    public ChessColor color;
    public List<LinearMove> moves;

    public GameObject modelToInstantiate;

    [HideInInspector]
    public GameObject model;
    private GameManager gm;
    private PieceData data;

    public void Initialize(Vector2Int pos, bool disableInstancing = false) {
        gm = FindObjectOfType<GameManager>();
        FillData(pos);
        if(gm.display && !disableInstancing) {
            float offset = 0.5f + gm.board.map[GetPosition().x][GetPosition().y].offsetY;
            model = Instantiate(modelToInstantiate, new Vector3(GetPosition().x, offset, GetPosition().y), Quaternion.identity, transform);
        }
    }

    public void FillData(Vector2Int pos) {
        data = new PieceData(type, color, moves, pos, gm, this);
        foreach (LinearMoveData move in data.moves) {
            move.AddCircularMove();
            move.pieceData = data;
        }
    }

    public void InitializeFromDataWithoutInstancing(PieceData d, GameObject model) {
        bool oldDisplay = gm.display;
        gm.display = false;
        InitializeFromData(d, model);
        gm.display = oldDisplay;
    }

    public void InitializeFromData(PieceData pieceData, GameObject modelToInstantiate) {
        gm = FindObjectOfType<GameManager>();
        data = pieceData;
        data.pieceVue = this;
        moves = new List<LinearMove>();
        foreach(LinearMoveData lmd in GetMoves()) {
            LinearMove lm = LinearMove.CreateAndAttachLinearMove(lmd, gameObject);
            moves.Add(lm);
        }
        this.modelToInstantiate = modelToInstantiate;
        if(gm.display) {
            float offset = 0.5f + gm.board.map[GetPosition().x][GetPosition().y].offsetY;
            this.model = Instantiate(modelToInstantiate, new Vector3(GetPosition().x, offset, GetPosition().y), Quaternion.identity, transform);
            this.model.name = "model";
        }
        color = pieceData.color;
        type = pieceData.type;
    }

    public IEnumerator MoveGameObject(Vector2Int dest, bool disableAnimation = false) {
        if (gm.display && !disableAnimation) {
            Vector3 destFinal = new Vector3(dest.x, gm.board.map[dest.x][dest.y].offsetY + 0.5f, dest.y);
            Vector3 source = model.transform.position;

            float startTime = Time.time;
            while (Time.time - startTime <= timeDeplacement)
            {
                float avancement = (Time.time - startTime) / timeDeplacement;
                Vector3 pos = source * (1.0f - avancement) + destFinal * avancement;
                model.transform.position = pos;
                yield return null;
            }

            model.transform.position = destFinal; // Comme ça c'est précis :D
        }
    }

    public void CaptureGameObject(bool disableAnimation = false) {
        if(!disableAnimation)
            AutoDestroy(disableAnimation);
    }

    public void AutoDestroy(bool disableAnimation = false) {
        // Se supprimer
        if(gm.display && !disableAnimation) {
            StartCoroutine(DestroyGameObject());
        } else {
            Destroy(this);
        }
    }

    private IEnumerator DestroyGameObject() {
        Vector3 direction = Vector3.up;

        // Il faudrait ici jouer l'animation de flammes ! <3

        float startTime = Time.time;
        while (Time.time - startTime <= timeDeath) {
            model.transform.Translate(direction * Time.deltaTime * speedDeath);
            yield return null;
        }

        Destroy(gameObject);
    }

    public PieceData GetData() {
        return data;
    }
    public PieceData GetDataCopy() {
        PieceData pd = new PieceData(data);
        return pd;
    }
    public void SetData(PieceData otherData) {
        data = otherData;
    }

    public GameObject GetModel() {
        return modelToInstantiate;
    }

    public PieceModelData GetModelData() {
        //if(data == null) {
        //    FillData(new Vector2Int(-1, -1));
        //    foreach (LinearMove move in moves)
        //        move.Initialize();
        //    data.SetMoves(moves);
        //}
        return new PieceModelData(GetDataCopy(), modelToInstantiate);
    }

    public PieceType GetPieceType() {
        return data.type;
    }
    public ChessColor GetColor() {
        return data.color;
    }
    public List<LinearMoveData> GetMoves() {
        return data.moves;
    }
    public Vector2Int GetPosition() {
        return data.position;
    }
    public bool GetHasMove() {
        return data.hasMove;
    }
    public void SetHasMove(bool newHasMove) {
        data.hasMove = newHasMove;
    }

    public string GetName() {
        return data.color + "_" + data.type;
    }
}
