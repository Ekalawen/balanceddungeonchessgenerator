﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic6x6PieceManager : PieceManager {

    public override void GeneratePieces() {
        piecesAtInit = new List<Piece>();
        GameObject go;
        Piece piece;

        // White
        // Pawns
        for(int i = 0; i < 6; i++) {
            go = Instantiate(whitePiecesModels[0].gameObject);
            piece = go.GetComponent<Piece>();
            piece.Initialize(new Vector2Int(1, i));
            piecesAtInit.Add(piece);
        }
        // Fous
        go = Instantiate(whitePiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 0));
        piecesAtInit.Add(piece);
        go = Instantiate(whitePiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 5));
        piecesAtInit.Add(piece);
        // Cavaliers
        go = Instantiate(whitePiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 1));
        piecesAtInit.Add(piece);
        go = Instantiate(whitePiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 4));
        piecesAtInit.Add(piece);
        // Tours
        go = Instantiate(whitePiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 2));
        piecesAtInit.Add(piece);
        // Roi
        go = Instantiate(whitePiecesModels[4].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 3));
        piecesAtInit.Add(piece);

        // Black
        // Pawns
        for(int i = 0; i < 6; i++) {
            go = Instantiate(blackPiecesModels[0].gameObject);
            piece = go.GetComponent<Piece>();
            piece.Initialize(new Vector2Int(4, i));
            piecesAtInit.Add(piece);
        }
        // Fous
        go = Instantiate(blackPiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(5, 0));
        piecesAtInit.Add(piece);
        go = Instantiate(blackPiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(5, 5));
        piecesAtInit.Add(piece);
        // Cavaliers
        go = Instantiate(blackPiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(5, 1));
        piecesAtInit.Add(piece);
        go = Instantiate(blackPiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(5, 4));
        piecesAtInit.Add(piece);
        // Tours
        go = Instantiate(blackPiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(5, 2));
        piecesAtInit.Add(piece);
        // Roi
        go = Instantiate(blackPiecesModels[4].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(5, 3));
        piecesAtInit.Add(piece);

        GeneratePiecesDataFromPiecesAtInit();
    }
}
