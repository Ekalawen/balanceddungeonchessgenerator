﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicPieceManager : PieceManager {

    public override void GeneratePieces() {
        piecesAtInit = new List<Piece>();
        GameObject go;
        Piece piece;

        // White
        // Pions
        for (int i = 0; i < 8; i++) {
            go = Instantiate(whitePiecesModels[0].gameObject);
            piece = go.GetComponent<Piece>();
            piece.Initialize(new Vector2Int(1, i));
            piecesAtInit.Add(piece);
        }
        // Fous
        go = Instantiate(whitePiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 2));
        piecesAtInit.Add(piece);
        go = Instantiate(whitePiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 5));
        piecesAtInit.Add(piece);
        // Cavaliers
        go = Instantiate(whitePiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 1));
        piecesAtInit.Add(piece);
        go = Instantiate(whitePiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 6));
        piecesAtInit.Add(piece);
        // Tours
        go = Instantiate(whitePiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 0));
        piecesAtInit.Add(piece);
        go = Instantiate(whitePiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 7));
        piecesAtInit.Add(piece);
        // Reine
        go = Instantiate(whitePiecesModels[4].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 4));
        piecesAtInit.Add(piece);
        // Roi
        go = Instantiate(whitePiecesModels[5].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(0, 3));
        piecesAtInit.Add(piece);

        // Black
        // Pions
        for (int i = 0; i < 8; i++) {
            go = Instantiate(blackPiecesModels[0].gameObject);
            piece = go.GetComponent<Piece>();
            piece.Initialize(new Vector2Int(6, i));
            piecesAtInit.Add(piece);
        }
        // Fous
        go = Instantiate(blackPiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 2));
        piecesAtInit.Add(piece);
        go = Instantiate(blackPiecesModels[1].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 5));
        piecesAtInit.Add(piece);
        // Cavaliers
        go = Instantiate(blackPiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 1));
        piecesAtInit.Add(piece);
        go = Instantiate(blackPiecesModels[2].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 6));
        piecesAtInit.Add(piece);
        // Tours
        go = Instantiate(blackPiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 0));
        piecesAtInit.Add(piece);
        go = Instantiate(blackPiecesModels[3].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 7));
        piecesAtInit.Add(piece);
        // Reine
        go = Instantiate(blackPiecesModels[4].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 4));
        piecesAtInit.Add(piece);
        // Roi
        go = Instantiate(blackPiecesModels[5].gameObject);
        piece = go.GetComponent<Piece>();
        piece.Initialize(new Vector2Int(7, 3));
        piecesAtInit.Add(piece);

        GeneratePiecesDataFromPiecesAtInit();
    }
}
