﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NDarray<T> {

    [SerializeField]public T[] t;
    [SerializeField]public Shape shape;

    public NDarray() {
        this.shape = new Shape(0);
        t = new T[0];
    }
    public NDarray(Shape shape) {
        this.shape = shape;
        t = new T[shape.nbElements];
    }
    public NDarray(int d1, int d2, int d3) : this(new Shape(new List<int> { d1, d2, d3 })) {
    }
    public NDarray(int d1, int d2) : this(new Shape(new List<int> { d1, d2 })) {
    }
    public NDarray(int d1) : this(new Shape(new List<int> { d1 })) {
    }
    public NDarray(List<T> values) {
        t = values.ToArray();
        shape = new Shape(values.Count);
    }
    public NDarray(T[] values) {
        t = values;
        shape = new Shape(values.Length);
    }
    public NDarray(NDarray<T> other) {
        this.shape = new Shape(other.shape);
        this.t = new T[other.shape.nbElements];
        for(int i = 0; i < GetNbElements(); i++) {
            this.t[i] = other.t[i];
        }
    }
    public int GetNbElements() {
        return shape.nbElements;
    }

    // As if the array was flatten, absolute indice
    public T this[int indice] {
        get => GetValue(indice);
        set => SetValue(indice, value);
    }
    public T GetValue(int indice) {
        return t[indice];
    }
    public void SetValue(int indice, T newValue) {
        t[indice] = newValue;
    }

    // All the indices are required !
    public T this[params int[] indices] {
        get => GetValue(indices);
        set => SetValue(indices, value);
    }
    public int GetIndice(int[] indices) {
        if (indices.Length != shape.GetNbDimentions())
            throw new System.Exception("Il doit y avoir autant d'indices que de dimensions !");
        int indice = 0;
        int facteurExpansion = shape.nbElements;
        for(int i = 0; i < indices.Length; i++) {
            facteurExpansion /= shape.GetDimension(i);
            indice += indices[i] * facteurExpansion;
        }
        return indice;
    }
    public T GetValue(int[] indices) {
        return t[GetIndice(indices)];
    }
    public void SetValue(int[] indices, T newValue) {
        t[GetIndice(indices)] = newValue;
    }

    // Only parts of the indices are required ... TODO !

    public NDarray<T> this[NDarray<int> indices] {
        get => SelectIndices(indices);
    }
    public NDarray<T> SelectIndices(NDarray<int> indices) {
        // Vérifier que les 2 shapes sont de dimension 1 ! :)
        if (indices.shape.GetNbDimentions() != 1 || shape.GetNbDimentions() != 1)
            throw new System.Exception("Lors d'une sélection d'incides, toutes les shapes doivent être de dimension 1 !");

        NDarray<T> newArray = new NDarray<T>(indices.GetNbElements());
        for(int i = 0; i < indices.GetNbElements(); i++) {
            newArray[i] = t[indices[i]];
        }
        return newArray;
    }

    public void Flatten() {
        shape = new Shape(GetNbElements());
    }

    public override string ToString() {
        string str = "";
        int nbDimensions = shape.GetNbDimentions();
        if(nbDimensions == 1 || nbDimensions > 3) {
            for(int i = 0; i < GetNbElements(); i++) {
                str += t[i] + " ";
            }
            str = str.Substring(0, str.Length - 1);
        } else if (nbDimensions == 2) {
            for(int i = 0; i < shape.GetD1(); i++) {
                for(int j = 0; j < shape.GetD2(); j++) {
                    str += this[i, j] + " ";
                }
                str += "\n";
            }
            str = str.Substring(0, str.Length - 1);
        } else { // nbDimensions == 3
            for (int k = 0; k < shape.GetD3(); k++) {
                for (int j = shape.GetD2() - 1; j >=0; j--) {
                    for (int i = 0; i < shape.GetD1(); i++) {
                        str += this[i, j, k] + " ";
                    }
                    str += "\n";
                }
                str += "\n";
            }
            str = str.Substring(0, str.Length - 1);
        }

        return str;
    }

    // Pour pouvoir appliquer une fonction à tous les membres de l'array
    public delegate T MapFonction(T input);
    public static NDarray<T> Map(NDarray<T> array, MapFonction mapFonction) {
        NDarray<T> res = new NDarray<T>(array.GetNbElements());
        for(int i = 0; i < array.GetNbElements(); i++) {
            res.t[i] = mapFonction(array.t[i]);
        }
        return res;
    }

    // Pour remplir avec une valeur
    public static void Fill(NDarray<T> array, T val) {
        for(int i = 0; i < array.GetNbElements(); i++) {
            array.t[i] = val;
        }
    }
}
