﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NDarrayF : NDarray<float>
{
    public NDarrayF() {
    }

    public NDarrayF(Shape shape) : base(shape) {
    }

    public NDarrayF(int d1) : base(d1) {
    }

    public NDarrayF(List<float> values) : base(values) {
    }

    public NDarrayF(float[] values) : base(values) {
    }

    public NDarrayF(NDarray<float> other) : base(other) {
    }

    public NDarrayF(int d1, int d2) : base(d1, d2) {
    }

    public NDarrayF(int d1, int d2, int d3) : base(d1, d2, d3) {
    }

    public static int Argmax(NDarrayF array) {
        if (array.GetNbElements() <= 0)
            return -1;
        int ind = 0;
        float maxValue = array[0];
        for(int i = 1; i < array.GetNbElements(); i++) {
            float val = array[i];
            if(val > maxValue) {
                ind = i;
                maxValue = array[i];
            }
        }
        return ind;
    }

    // Pour pouvoir additionner deux NDarray membres à membre
    public static NDarrayF Plus(NDarrayF a, NDarrayF b) {
        if (!a.shape.Equals(b.shape))
            throw new System.Exception("Pour additionner 2 NDarray, ils doivent être de même dimensions !");

        NDarrayF res = new NDarrayF(a);
        for (int i = 0; i < a.GetNbElements(); i++)
            res[i] += b[i];
        return res;
    }

    // Pour pouvoir soustraire deux NDarray membres à membre
    public static NDarrayF Minus(NDarrayF a, NDarrayF b) {
        if (!a.shape.Equals(b.shape))
            throw new System.Exception("Pour soustraire 2 NDarray, ils doivent être de même dimensions !");

        NDarrayF res = new NDarrayF(a);
        for (int i = 0; i < a.GetNbElements(); i++)
            res[i] -= b[i];
        return res;
    }

    // Pour pouvoir multiplier deux NDarray membres à membre
    public static NDarrayF Mult(NDarrayF a, NDarrayF b) {
        if (!a.shape.Equals(b.shape))
            throw new System.Exception("Pour multiplier 2 NDarray, ils doivent être de même dimensions !");

        NDarrayF res = new NDarrayF(a);
        for (int i = 0; i < a.GetNbElements(); i++)
            res[i] *= b[i];
        return res;
    }

    // Pour pouvoir multiplier un NDarray par un scalaire
    public static NDarrayF Mult(NDarrayF a, float scal) {
        NDarrayF res = new NDarrayF(a);
        for (int i = 0; i < a.GetNbElements(); i++)
            res[i] *= scal;
        return res;
    }

    // Pour calculer le outer product de 2 array
    public static NDarrayF Outer(NDarrayF left, NDarrayF right) {
        if (left.shape.GetNbDimentions() != 1 || right.shape.GetNbDimentions() != 1)
            throw new System.Exception("Le outer product de 2 array nécessite que chacun de ces array soit de dimension 1 !");

        NDarrayF res = new NDarrayF(new Shape(right.GetNbElements(), left.GetNbElements()));
        for(int i = 0; i < res.shape.GetD1(); i++) {
            for (int j = 0; j < res.shape.GetD2(); j++) {
                res[i, j] = right[i] * left[j];
            }
        }
        return res;
    }

    // Pour calculer le dot product de 2 array
    public static NDarrayF Dot(NDarrayF left, NDarrayF right) {
        // Produit de vecteurs, renvoie un scalaire
        if(left.shape.GetNbDimentions() == 1 && right.shape.GetNbDimentions() == 1) {
            if (left.GetNbElements() != right.GetNbElements())
                throw new System.Exception("Le dot product de 2 vecteurs doit se faire entre vecteurs de même taille !");
            NDarrayF dot = new NDarrayF(1); // un réel
            for (int i = 0; i < left.GetNbElements(); i++)
                dot[0] += left[i] * right[i];
            return dot;
        }

        if(left.shape.GetNbDimentions() == 2 && right.shape.GetNbDimentions() == 2) {
            if(left.shape.GetNbDimentions() != right.shape.GetNbDimentions())
                throw new System.Exception("Le dot product de 2 matrices doit se faire entre matrices de bonne taille !");
            NDarrayF res = new NDarrayF(left.shape.GetD1(), right.shape.GetD2());
            for(int i = 0; i < res.shape.GetD1(); i++) {
                for(int j = 0; j < res.shape.GetD2(); j++) {
                    for(int k = 0; k < left.shape.GetD2(); k++) {
                        res[i, j] += left[i, k] * right[k, j];
                    }
                }
            }
            return res;
        }

        if(left.shape.GetNbDimentions() == 1 && right.shape.GetNbDimentions() == 2) {
            if(left.GetNbElements() != right.shape.GetD1())
                throw new System.Exception("Le dot product d'un vecteur et d'une matrice doit se faire avec de bonnes tailles !");
            NDarrayF res = new NDarrayF(right.shape.GetD2());
            for(int i = 0; i < res.shape.GetD1(); i++) {
                for(int k = 0; k < left.GetNbElements(); k++) {
                    res[i] += left[k] * right[i, k];
                }
            }
            return res;
        }

        if(left.shape.GetNbDimentions() == 2 && right.shape.GetNbDimentions() == 1) {
            if(left.shape.GetD2() != right.GetNbElements())
                throw new System.Exception("Le dot product d'une matrice et d'un vecteur doit se faire avec de bonnes tailles !");
            NDarrayF res = new NDarrayF(left.shape.GetD1());
            for(int i = 0; i < res.shape.GetD1(); i++) {
                for(int k = 0; k < right.GetNbElements(); k++) {
                    res[i] += left[i, k] * right[k];
                }
            }
            return res;
        }

        throw new System.Exception("Le dot product doit se faire, soit entre 2 vecteurs, soit entre 2 matrices, soit entre 1 vecteur et une matrice !!");
    }

    // Pour transposer une matrice de dimension 2
    public static void Transpose(NDarrayF matrice) {
        // TODO !
        throw new System.Exception("Il faut implémenter la fonction Transpose ! <3");
    }

    // Pour calculer le maximum
    public static float Max(NDarrayF a, int indStart = -1, int indEnd = -1) {
        if(a.GetNbElements() <= 0)
            throw new System.Exception("Il faut au moins 1 élément pour calculer un maximum !");
        if (indStart == -1)
            indStart = 0;
        if (indEnd == -1)
            indEnd = a.GetNbElements();

        float max = a.t[indStart];
        for(int i = indStart + 1; i < indEnd; i++) {
            if(a.t[i] > max) {
                max = a.t[i];
            }
        }
        return max;
    }

    // Pour calculer le minimum
    public static float Min(NDarrayF a, int indStart = -1, int indEnd = -1) {
        if(a.GetNbElements() <= 0)
            throw new System.Exception("Il faut au moins 1 élément pour calculer un minimum !");
        if (indStart == -1)
            indStart = 0;
        if (indEnd == -1)
            indEnd = a.GetNbElements();

        float min = a.t[indStart];
        for(int i = indStart + 1; i < indEnd; i++) {
            if(a.t[i] < min) {
                min = a.t[i];
            }
        }
        return min;
    }

    // Pour trouver l'indice du maximum
    public static int Argmax(NDarrayF a, int indStart = -1, int indEnd = -1) {
        if(a.GetNbElements() <= 0)
            throw new System.Exception("Il faut au moins 1 élément pour calculer un maximum !");
        if (indStart == -1)
            indStart = 0;
        if (indEnd == -1)
            indEnd = a.GetNbElements();

        int indMax = indStart;
        float max = a.t[indStart];
        for(int i = indStart + 1; i < indEnd; i++) {
            if(a.t[i] > max) {
                indMax = i;
                max = a.t[i];
            }
        }
        return indMax;
    }

    // Pour calculer le minimum
    public static int Argmin(NDarrayF a, int indStart = -1, int indEnd = -1) {
        if(a.GetNbElements() <= 0)
            throw new System.Exception("Il faut au moins 1 élément pour calculer un minimum !");
        if (indStart == -1)
            indStart = 0;
        if (indEnd == -1)
            indEnd = a.GetNbElements();

        int indMin = indStart;
        float min = a.t[indStart];
        for(int i = indStart + 1; i < indEnd; i++) {
            if(a.t[i] < min) {
                indMin = i;
                min = a.t[i];
            }
        }
        return indMin;
    }

    // Pour pouvoir appliquer une fonction à tous les membres de l'array
    public delegate float MapFonction(float input);
    public static NDarrayF Map(NDarrayF array, MapFonction mapFonction) {
        NDarrayF res = new NDarrayF(array.GetNbElements());
        for(int i = 0; i < array.GetNbElements(); i++) {
            res.t[i] = mapFonction(array.t[i]);
        }
        return res;
    }

    // Pour remplir avec une valeur
    public static void Fill(NDarrayF array, float val) {
        for(int i = 0; i < array.GetNbElements(); i++) {
            array.t[i] = val;
        }
    }

    public NDarrayF this[NDarray<int> indices] {
        get => SelectIndices(indices);
    }
    public NDarrayF SelectIndices(NDarray<int> indices) {
        // Vérifier que les 2 shapes sont de dimension 1 ! :)
        if (indices.shape.GetNbDimentions() != 1 || shape.GetNbDimentions() != 1)
            throw new System.Exception("Lors d'une sélection d'incides, toutes les shapes doivent être de dimension 1 !");

        NDarrayF newArray = new NDarrayF(indices.GetNbElements());
        for(int i = 0; i < indices.GetNbElements(); i++) {
            newArray[i] = t[indices[i]];
        }
        return newArray;
    }
}
