﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ModelSequential {

    [SerializeField]public List<Layer> layers;

    public ModelSequential() {
        layers = new List<Layer>();
    }

    public void Add(Layer layer) {
        layers.Add(layer);
        layer.OnAddToModel(this);
    }

    public NDarrayF Predict(NDarrayF input) {
        if(!input.shape.Equals(FirstLayer().shape)) {
            throw new System.Exception("L'input doit être de même dimension que le premier layer !");
        }

        NDarrayF inputCopy = new NDarrayF(input);
        for(int i = 0; i < layers.Count; i++) {
            inputCopy = layers[i].ComputeForward(inputCopy);
        }

        return inputCopy;
    }

    public void Train(List<TrainingData> trainingDatas, 
                      float learningRate,
                      int steps,
                      int batchSize,
                      int nbEvaluations = 10) {
        int nbDatas = trainingDatas.Count;
        for (int i = 0; i < steps; i++) {
            // On shuffle pour que les gradients soient toujours différents !
            Shuffle(trainingDatas);

            // Pour chaque batch
            for (int j = 0; j < nbDatas; j += batchSize) {

                // On le récupère
                List<TrainingData> batchDatas = new List<TrainingData>();
                for (int k = j; k < j + batchSize && k < nbDatas; k++) {
                    batchDatas.Add(trainingDatas[k]);
                }

                // Puis on l'entraîne !
                TrainBatch(batchDatas, learningRate);
            }

            // Evaluate
            if((i + 1) % (steps / nbEvaluations) == 0 || i == steps - 1)
                Evaluate(trainingDatas, numTraining: i + 1);
        }
    }

    private void TrainBatch(List<TrainingData> trainingDatas, float learningRate) {
        // On commence par une version facile, on fera les moyennes après !
        /// TODO !
        for(int i = 0; i < trainingDatas.Count; i++) {
            Backpropagation(trainingDatas[i], learningRate);
        }
    }

    public void Backpropagation(TrainingData trainingData, float learningRate) {
        // Passe vers l'avant en retenant les résultats intermédiaires
        // Chaque réseau retient ce dont il a besoin de manière autonome
        int N = layers.Count;
        NDarrayF input = new NDarrayF(trainingData.input);
        for(int i = 0; i < N; i++) {
            input = layers[i].ComputeForward(input);
        }

        // Calcul de l'erreur
        NDarrayF output = input;
        NDarrayF loss = ComputeLoss(output, trainingData.expectedOutput);
        NDarrayF loss_diff_output = ComputeLossDerivative(output, trainingData.expectedOutput);

        // On va maintenant rétropropager notre gradient à travers tous les layers !
        NDarrayF gradient = loss_diff_output;
        for(int i = N - 1; i >= 0; i--) {
            // Et c'est le layer qui met à jour lui-même ses paramètres s'il le faut !
            gradient = layers[i].ComputeBackward(gradient, learningRate);
        }
    }

    public static void Shuffle<T>(List<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    public NDarrayF ComputeLoss(NDarrayF output, NDarrayF expectedOutput) {
        // We will use cross entropy for all value
        // except the last where we will use a mean-squared error
        int N = output.GetNbElements();
        float crossEntropy = 0f;
        for(int i = 0; i < N - 1; i++) {
            crossEntropy -= output[i] * Mathf.Log(expectedOutput[i]);
        }
        float mseWinRate = Mathf.Pow(expectedOutput[N - 1] - output[N - 1], 2);

        NDarrayF res = new NDarrayF(2);
        res[0] = crossEntropy;
        res[1] = mseWinRate;
        return res;
    }

    public NDarrayF ComputeLossDerivative(NDarrayF output, NDarrayF expectedOutput) {
        NDarrayF derivative = new NDarrayF(output);
        derivative = NDarrayF.Minus(derivative, expectedOutput);
        int N = derivative.GetNbElements();
        derivative[N - 1] = - 2 * (expectedOutput[N - 1] - output[N - 1]);
        return derivative;
    }

    public NDarrayF ComputeLossDerivativeGeneric(NDarrayF output, NDarrayF expectedOutput) {
        NDarrayF derivative = new NDarrayF(output);
        derivative = NDarrayF.Minus(derivative, expectedOutput);
        return derivative;
    }

    public Layer FirstLayer() {
        if(layers.Count == 0)
            throw new System.Exception("Il n'y a pas de layers dans ce modèle !");
        return layers[0];
    }

    public void Evaluate(List<TrainingData> trainingDatas, int numTraining = -1) {
        // Compute rates
        int nbSuccess = 0;
        float mseWinRate = 0;
        int N = trainingDatas[0].expectedOutput.GetNbElements();
        for (int i = 0; i < trainingDatas.Count; i++) {
            NDarrayF output = Predict(trainingDatas[i].input);
            nbSuccess += (IsGood(output, trainingDatas[i].expectedOutput)) ? 1 : 0;
            mseWinRate += Mathf.Pow(trainingDatas[i].expectedOutput[N - 1] - output[N - 1], 2);
        }
        float successRate = (float)nbSuccess / (float)trainingDatas.Count;
        float winRateError = (float)mseWinRate / (float)trainingDatas.Count;

        // Print
        string strNum = ((numTraining != -1) ? (numTraining + " : ") : "");
        Debug.Log(strNum + "Success Rate = " + (nbSuccess) + "/" + trainingDatas.Count
                + " Win Rate Error = " + winRateError);
    }

    public void EvaluateGeneric(List<TrainingData> trainingDatas, int numTraining = -1) {
        // Compute rates
        int nbSuccess = 0;
        int N = trainingDatas[0].expectedOutput.GetNbElements();
        for (int i = 0; i < trainingDatas.Count; i++) {
            NDarrayF output = Predict(trainingDatas[i].input);
            int indOutput = NDarrayF.Argmax(output);
            int indExpectedOutput = NDarrayF.Argmax(trainingDatas[i].expectedOutput);
            nbSuccess += (indOutput == indExpectedOutput) ? 1 : 0;
        }
        float successRate = (float)nbSuccess / (float)trainingDatas.Count;

        // Print
        string strNum = ((numTraining != -1) ? (numTraining + " : ") : "");
        Debug.Log(strNum + "Success Rate = " + (nbSuccess) + " / " + trainingDatas.Count);
    }


    public bool IsGood(NDarrayF output, NDarrayF expectedOutput) {
        return GetIndiceBestPlayFromOutput(output) == GetIndiceBestPlayFromOutput(expectedOutput);
    }

    public int GetIndiceBestPlayFromOutput(NDarrayF output) {
        return NDarrayF.Argmax(output, 0, output.GetNbElements() - 1);
    }

    public float GetWinRateFromOutput(NDarrayF output) {
        return output[output.GetNbElements() - 1];
    }

    public ModelSequentialData GetData() {
        ModelSequentialData data = new ModelSequentialData();
        data.denseLayers = new List<DenseLayer>();
        data.inputLayers = new List<InputLayer>();
        data.flattenLayers = new List<FlattenLayer>();
        data.softmaxLayers = new List<SoftMaxLayer>();
        data.layerOrder = new List<LayerType>();

        foreach(Layer layer in layers) {
            data.layerOrder.Add(layer.layerType);
            switch(layer.layerType) {
                case LayerType.DENSE:
                    data.denseLayers.Add((DenseLayer)layer);
                    break;
                case LayerType.FLATTEN:
                    data.flattenLayers.Add((FlattenLayer)layer);
                    break;
                case LayerType.INPUT:
                    data.inputLayers.Add((InputLayer)layer);
                    break;
                case LayerType.SOFTMAX:
                    data.softmaxLayers.Add((SoftMaxLayer)layer);
                    break;
                default:
                    throw new System.Exception("On a oublié de gérer un type de layer !");
            }
        }

        return data;
    }

    public static ModelSequential GetModelFromData(ModelSequentialData data) {
        ModelSequential model = new ModelSequential();

        int[] indicesLayers = new int[System.Enum.GetValues(typeof(LayerType)).Length];

        int ind;
        foreach(LayerType layerType in data.layerOrder) {
            switch(layerType) {
                case LayerType.DENSE:
                    ind = indicesLayers[(int)layerType];
                    data.denseLayers[ind].InitializeActivationFunction();
                    model.Add(data.denseLayers[ind]);
                    indicesLayers[(int)layerType]++;
                    break;
                case LayerType.FLATTEN:
                    ind = indicesLayers[(int)layerType];
                    model.Add(data.flattenLayers[ind]);
                    indicesLayers[(int)layerType]++;
                    break;
                case LayerType.INPUT:
                    ind = indicesLayers[(int)layerType];
                    model.Add(data.inputLayers[ind]);
                    indicesLayers[(int)layerType]++;
                    break;
                case LayerType.SOFTMAX:
                    ind = indicesLayers[(int)layerType];
                    model.Add(data.softmaxLayers[ind]);
                    indicesLayers[(int)layerType]++;
                    break;
                default:
                    throw new System.Exception("GetModelFromData() : On a oublié de gérer un type de layer !");
            }
        }

        return model;
    }
}
