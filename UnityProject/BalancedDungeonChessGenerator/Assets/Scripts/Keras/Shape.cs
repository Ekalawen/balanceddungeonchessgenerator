﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Shape {
    [SerializeField]public List<int> sizes;
    [SerializeField]public int nbElements;

    public Shape(List<int> sizes) {
        this.sizes = sizes;
        if (sizes == null || sizes.Count == 0)
            nbElements = 0;
        int prod = 1;
        foreach (int size in sizes)
            prod *= size;
        nbElements = prod;
    }
    public Shape(int d1, int d2, int d3) : this(new List<int>() { d1, d2, d3 }) {
    }
    public Shape(int d1, int d2) : this(new List<int>() { d1, d2 }) {
    }
    public Shape(int d1) : this(new List<int>() { d1 }) {
    }
    public Shape(Shape other) {
        this.nbElements = other.nbElements;
        this.sizes = new List<int>();
        for(int i = 0; i < other.sizes.Count; i++) {
            this.sizes.Add(other.sizes[i]);
        }
    }
    public int GetNbDimentions() {
        return sizes.Count;
    }
    public int GetD1() {
        return sizes[0];
    }
    public int GetD2() {
        return sizes[1];
    }
    public int GetD3() {
        return sizes[2];
    }
    public int GetDimension(int dimensionIndice) {
        return sizes[dimensionIndice];
    }

    public override string ToString() {
        string s = "(";
        for(int i = 0; i < sizes.Count; i++) {
            s += sizes[i];
            if (i != sizes.Count - 1)
                s += ", ";
        }
        s += ")";
        return s;
    }

    // On assume pour le moment que ça fonctionne, mais à vérifier !
    public override bool Equals(object obj) {
        var shape = obj as Shape;
        if (shape == null)
            return false;
        bool equal = sizes.Count == shape.sizes.Count;
        if (equal) {
            for (int i = 0; i < sizes.Count; i++) {
                if (sizes[i] != shape.sizes[i]) {
                    equal = false;
                    break;
                }
            }
        }
        return equal;
    }

    public override int GetHashCode() {
        return 329952645 + EqualityComparer<List<int>>.Default.GetHashCode(sizes);
    }
}
