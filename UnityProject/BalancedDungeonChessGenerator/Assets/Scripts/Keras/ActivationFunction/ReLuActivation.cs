﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReLuActivation : ActivationFunction
{
    public ReLuActivation() {
        typeActivation = TypeActivationFunction.RELU;
    }

    public override float Compute(float input) {
        return Mathf.Max(0, input);
    }

    public override float ComputeDerivative(float input) {
        return (input > 0) ? 1 : 0;
    }
}