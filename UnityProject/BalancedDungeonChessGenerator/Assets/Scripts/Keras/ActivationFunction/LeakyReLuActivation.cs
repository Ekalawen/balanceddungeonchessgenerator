﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeakyReLuActivation : ActivationFunction {

    public static float COEF = 0.01f;

    public LeakyReLuActivation() {
        typeActivation = TypeActivationFunction.LEAKY_RELU;
    }

    public override float Compute(float input) {
        //return (input > 0) ? input : COEF * input;
        return Mathf.Max(input, COEF * input); // ONLY IF 0 <= COEF <= 1 !
    }

    public override float ComputeDerivative(float input) {
        return (input > 0) ? 1 : COEF;
    }
}
