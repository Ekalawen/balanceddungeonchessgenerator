﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SigmoidActivation : ActivationFunction {

    public SigmoidActivation() {
        typeActivation = TypeActivationFunction.SIGMOID;
    }

    public override float Compute(float input) {
        float exp = Mathf.Exp(-input);
        return 1.0f / (1.0f + exp);
    }

    public override float ComputeDerivative(float input) {
        float sigmoidInput = Compute(input);
        return sigmoidInput * (1 - sigmoidInput); // C'est la plus belle dérivée du monde !
    }
}
