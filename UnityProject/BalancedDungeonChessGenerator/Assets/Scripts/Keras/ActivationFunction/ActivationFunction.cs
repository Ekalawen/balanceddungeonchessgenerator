﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeActivationFunction { SIGMOID, RELU, LEAKY_RELU};

public abstract class ActivationFunction {
    public TypeActivationFunction typeActivation;

    public abstract float Compute(float input);

    public abstract float ComputeDerivative(float input);
}
