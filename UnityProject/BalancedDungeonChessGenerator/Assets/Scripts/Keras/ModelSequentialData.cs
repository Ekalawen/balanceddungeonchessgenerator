﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LayerType { DENSE, FLATTEN, INPUT, SOFTMAX, BATCH_NORMALIZATION };

[Serializable]
public struct ModelSequentialData {
    [SerializeField] public List<DenseLayer> denseLayers;
    [SerializeField] public List<FlattenLayer> flattenLayers;
    [SerializeField] public List<InputLayer> inputLayers;
    [SerializeField] public List<SoftMaxLayer> softmaxLayers;
    [SerializeField] public List<LayerType> layerOrder;
}
