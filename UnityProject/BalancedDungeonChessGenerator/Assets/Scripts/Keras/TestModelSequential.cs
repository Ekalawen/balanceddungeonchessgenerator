﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// On va générer des tableaux de petite taille en on hot
// Et la sortie attendu sera la fonction identité (donc le même vecteur que en entrée)
// Ce sera utile pour débugger la gradient descent !
public class TestModelSequential {

    int N = 4;
    int nbDatas = 100;
    ModelSequential model;

    public void Run() {
        // Générer le model
        model = new ModelSequential();
        model.Add(new InputLayer(new Shape(N)));
        //DenseLayer denseLayer = new DenseLayer(10, activationFunction: new ReLuActivation());
        //model.Add(denseLayer);
        model.Add(new DenseLayer(20, activationFunction: new LeakyReLuActivation()));
        model.Add(new DenseLayer(20, activationFunction: new LeakyReLuActivation()));
        model.Add(new DenseLayer(20, activationFunction: new LeakyReLuActivation()));
        model.Add(new DenseLayer(N, activationFunction: new LeakyReLuActivation()));
        model.Add(new SoftMaxLayer(0, N));

        // Générer les training datas
        List<TrainingData> trainingDatas = GenerateTrainingDatas();

        // Train
        //Debug.Log("denseLayer.weights = \n" + denseLayer.weights);
        model.Train(trainingDatas, learningRate: 0.01f, steps: 100, batchSize: 50);
        //Debug.Log("denseLayer.weights = \n" + denseLayer.weights)
    }

    public List<TrainingData> GenerateTrainingDatas() {
        List<TrainingData> trainingDatas = new List<TrainingData>();
        for(int i = 0; i < nbDatas; i++) {
            TrainingData data = new TrainingData();
            data.input = new NDarrayF(N);
            data.input[Random.Range(0, N)] = 1.0f;
            data.expectedOutput = new NDarrayF(data.input);
            trainingDatas.Add(data);
        }

        return trainingDatas;
    }

}
