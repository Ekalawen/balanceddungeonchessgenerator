﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Neuron {

    public static float MIN_WEIGHT = -1.0f;
    public static float MAX_WEIGHT = 1.0f;
    public static float MIN_TRESHOLD = 0.0f;
    public static float MAX_TRESHOLD = 0.0f;

    [SerializeField]public float[] weights;
    [SerializeField]public float threshold;

    public Neuron(int nbConnexions) {
        weights = new float[nbConnexions];
        for(int i = 0; i < nbConnexions; i++) {
            weights[i] = UnityEngine.Random.Range(MIN_WEIGHT, MAX_WEIGHT);
        }
        threshold = UnityEngine.Random.Range(MIN_TRESHOLD, MAX_TRESHOLD);
    }
}
