﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Cette structure contiendra toutes les informations potentiellements utiles
// pour un gradient de n'importe quel layer
public struct GradientData {
    public float[][] gradientWeight;
    public float[] gradientBias;
    // ...
}
