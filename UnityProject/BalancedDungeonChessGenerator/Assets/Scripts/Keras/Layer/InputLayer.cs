﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InputLayer : Layer {

    public InputLayer(Shape inputShape) {
        this.shape = inputShape;
        this.layerType = LayerType.INPUT;
    }

    public override NDarrayF ComputeForward(NDarrayF input) {
        return input;
    }

    public override NDarrayF ComputeBackward(NDarrayF gradient, float learningRate) {
        return gradient;
    }


    public override void OnAddToModel(ModelSequential model) {
        // Nothing to do !
    }
}
