﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BatchNormalizationLayer : Layer {

    [SerializeField] private float gamma = 1.0f;
    [SerializeField] private float beta = 0.0f;

    public BatchNormalizationLayer() {
        this.layerType = LayerType.BATCH_NORMALIZATION;
    }

    public override void OnAddToModel(ModelSequential model) {
    }

    public override NDarrayF ComputeForward(NDarrayF input) {
        throw new System.NotImplementedException();
    }

    public override NDarrayF ComputeBackward(NDarrayF gradient, float learningRate) {
        throw new System.NotImplementedException();
    }

}
