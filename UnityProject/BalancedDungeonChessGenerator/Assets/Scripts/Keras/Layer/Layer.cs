﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Layer {

    [SerializeField]public Shape shape;
    [SerializeField]public LayerType layerType;

    public abstract NDarrayF ComputeForward(NDarrayF input);

    public abstract NDarrayF ComputeBackward(NDarrayF gradient, float learningRate);

    public abstract void OnAddToModel(ModelSequential model);
}
