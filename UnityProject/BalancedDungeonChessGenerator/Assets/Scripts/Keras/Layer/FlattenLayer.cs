﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FlattenLayer : Layer {

    [SerializeField]private Shape previousShape;

    public FlattenLayer() {
        layerType = LayerType.FLATTEN;
    }

    public override NDarrayF ComputeForward(NDarrayF input) {
        input.Flatten();
        return input;
    }

    public override NDarrayF ComputeBackward(NDarrayF gradient, float learningRate) {
        gradient.shape = new Shape(previousShape);
        return gradient;
    }

    public override void OnAddToModel(ModelSequential model) {
        if (model.layers.Count <= 1)
            throw new System.Exception("Un FlattenLayer ne peut pas être le premier dans un modèle !");
        previousShape = model.layers[model.layers.Count - 2].shape;
        shape = new Shape(previousShape.nbElements);
    }
}
