﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SoftMaxLayer : Layer {

    [SerializeField]int indStart; // [indStart, indEnd[
    [SerializeField]int indEnd;

    [NonSerialized] private NDarrayF inputCache;
    [NonSerialized] private NDarrayF outputCache;
    [NonSerialized] private float sommeCache;

    public SoftMaxLayer(int indStart, int indEnd) {
        this.indStart = indStart;
        this.indEnd = indEnd;
        this.layerType = LayerType.SOFTMAX;
    }

    public override NDarrayF ComputeForward(NDarrayF input) {
        if (input.shape.GetNbDimentions() != 1)
            throw new System.Exception("Le layer précédent d'un SoftMaxLayer doit être de dimension 1 !");
        if(indStart < 0 || indEnd > input.shape.nbElements)
            throw new System.Exception("Les valeurs indStart et indEnd doivent être inclus dans la taille du layer précédent !");

        // On retient l'entrée ! :)
        inputCache = new NDarrayF(input);

        // D is just here to prevent exp to go too big, but doesn't change the calcul !
        float D = - NDarrayF.Max(input);

        sommeCache = 0;
        for (int i = indStart; i < indEnd; i++) {
            sommeCache += Mathf.Exp(input[i] + D);
        }

        outputCache = new NDarrayF(input.shape);
        for(int i = 0; i < input.GetNbElements(); i++) {
            if(indStart <= i && i < indEnd) {
                outputCache[i] = Mathf.Exp(input[i] + D) / sommeCache;
            }
        }

        return new NDarrayF(outputCache);
    }

    public override NDarrayF ComputeBackward(NDarrayF gradient, float learningRate) {
        NDarrayF finalGradient = new NDarrayF(gradient); // finalGradient = Dot(jacobian, gradient) si dans les bornes, identity sinon
        for(int i = indStart; i < indEnd; i++) {
            finalGradient[i] = finalGradient[i] * (outputCache[i] * (1 - outputCache[i]));
        }
        return finalGradient;

        //// On va calculer le finalGradient directement pour éviter de gaspiller trop de mémoire
        //// en explicitant la jacobienne !
        //NDarrayF finalGradient = new NDarrayF(gradient); // finalGradient = Dot(jacobian, gradient) si dans les bornes, identity sinon
        //for(int k = indStart; k < indEnd; k++) {
        //    float somme = 0;
        //    for(int i = indStart; i < indEnd; i++) {
        //        float deltaIK = (i == k) ? 1 : 0;
        //        float jacobianIK = outputCache[i] * (deltaIK - outputCache[k]);
        //        somme += jacobianIK;
        //    }
        //    finalGradient[k] = somme * gradient[k];
        //}
        //return finalGradient;

        //// Juste, mais le calcul de la jacobienne est trop gourmand en mémoire !
        //NDarrayF jacobian = new NDarrayF(N, N);
        //for(int i = 0; i < N; i++) {
        //    for(int j = 0; j < N; j++) {
        //        float deltaIJ = (i == j) ? 1 : 0;
        //        jacobian[i, j] = outputCache[i] * (deltaIJ - outputCache[j]);
        //    }
        //}
        //return NDarrayF.Dot(jacobian, gradient);
    }

    public override void OnAddToModel(ModelSequential model) {
    }
}
