﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DenseLayer : Layer {

    public float min_weight = -1.0f;
    public float max_weight = 1.0f;
    public float min_bias = 0.0f;
    public float max_bias = 0.0f;

    [SerializeField]private int nbNeurons;
    [SerializeField]private int nbConnexions;
    //[SerializeField]private List<Neuron> neurons;
    [SerializeField]public NDarrayF weights; // 2 dimensions : D1 = weight of the neuron, D2 = neuron
    [SerializeField]public NDarrayF bias; // 1 dimensions
    [SerializeField]private Shape previousShape;
    [NonSerialized] private ActivationFunction activationFunction;
    [SerializeField]public TypeActivationFunction typeActivationFunction;

    [NonSerialized] private NDarrayF inputCache;
    [NonSerialized] private NDarrayF aggregationCache;

    public DenseLayer(int nbNeurons, ActivationFunction activationFunction) {
        layerType = LayerType.DENSE;
        typeActivationFunction = activationFunction.typeActivation;
        shape = new Shape(nbNeurons);
        this.nbNeurons = nbNeurons;
        this.activationFunction = activationFunction;
    }

    public override void OnAddToModel(ModelSequential model) {
        if (model.layers.Count <= 1)
            throw new System.Exception("Un DenseLayer ne peut pas être le premier dans un modèle !");
        Shape previousShape = model.layers[model.layers.Count - 2].shape;
        this.previousShape = previousShape;
        if (previousShape.GetNbDimentions() != 1)
            throw new System.Exception("Un DenseLayer doit être précédé d'un layer à 1 dimension !");

        nbConnexions = previousShape.GetD1();
        if (weights == null || weights.GetNbElements() == 0) { // We don't want to initialize weights again if we load a model
            weights = new NDarrayF(nbConnexions, nbNeurons);
            bias = new NDarrayF(nbNeurons);
            GlorotInitializer(nbConnexions, nbNeurons);
            for (int i = 0; i < nbNeurons; i++)
            {
                for (int j = 0; j < nbConnexions; j++)
                {
                    weights[j, i] = UnityEngine.Random.Range(min_weight, max_weight);
                }
                bias[i] = UnityEngine.Random.Range(min_bias, max_bias);
            }
        }
    }

    private void GlorotInitializer(int nbIn, int nbOut) {
        int nbFans = nbIn + nbOut;
        float sqrtFans = Mathf.Sqrt(6.0f / (float)nbFans);
        min_weight = - sqrtFans;
        max_weight = sqrtFans;
        min_bias = 0.0f;
        max_bias = 0.0f;
    }

    public override NDarrayF ComputeForward(NDarrayF input) {
        if(!previousShape.Equals(input.shape) || input.shape.GetNbDimentions() != 1)
            throw new System.Exception("L'input d'un layer doit être la même que celle de son previous layer et être de dimension 1 !\nAttendu = " + previousShape + "\nReçu = " + input.shape);

        // We keep theses in memory for the backpropagation part
        inputCache = new NDarrayF(input);
        aggregationCache = new NDarrayF(nbNeurons);
        NDarrayF activation = new NDarrayF(nbNeurons); // we don't need the activation in cache

        for(int i = 0; i < nbNeurons; i++) {
            float somme = 0;
            for (int j = 0; j < nbConnexions; j++) {
                somme += input[j] * weights[j, i];
            }
            aggregationCache[i] = somme + bias[i];
            activation[i] = activationFunction.Compute(aggregationCache[i]);
        }

        return activation;
    }

    public override NDarrayF ComputeBackward(NDarrayF loss_diff_a, float learningRate) {
        // Compute gradients
        NDarrayF a_diff_z = NDarrayF.Map(aggregationCache, activationFunction.ComputeDerivative);
        NDarrayF z_diff_w = inputCache;
        NDarrayF localGradient = NDarrayF.Mult(loss_diff_a, a_diff_z); // produit de vecteurs termes à termes pour simplifier les calculs
        NDarrayF weightGradient = NDarrayF.Outer(localGradient, z_diff_w); // 2 dimensions !
        NDarrayF tresholdGradient = localGradient;

        // Update Parameters
        UpdateParameters(weightGradient, tresholdGradient, learningRate);

        // Renvoyer le gradient "sortant"
        NDarrayF mul = NDarrayF.Mult(loss_diff_a, a_diff_z);
        NDarrayF dot = NDarrayF.Dot(weights, mul);
        return dot;
    }

    public void InitializeActivationFunction() {
        switch(typeActivationFunction) {
            case TypeActivationFunction.SIGMOID:
                activationFunction = new SigmoidActivation();
                break;
            case TypeActivationFunction.RELU:
                activationFunction = new ReLuActivation();
                break;
            case TypeActivationFunction.LEAKY_RELU:
                activationFunction = new LeakyReLuActivation();
                break;
            default:
                throw new System.Exception("Activation Function inconnu ! " + typeActivationFunction);
        }
    }

    public void UpdateParameters(NDarrayF weightsGradient, NDarrayF biasGradient, float learningRate) {
        for (int i = 0; i < nbNeurons; i++) {
            // Le biais
            bias[i] = bias[i] - learningRate * biasGradient[i];

            // Les weights
            for (int j = 0; j < nbConnexions; j++) {
                weights[j, i] = weights[j, i] - learningRate * weightsGradient[j, i];
            }
        }
    }
}
