﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanPlayer : Player {

    public Color deplacementColor = Color.red;
    public Color captureColor = Color.blue;

    private GameManager gm;
    private Piece pieceSelectionne = null;
    private Vector2Int cibleSelectionne;
    private bool bCibleSelectionne = false;
    private Color oldColor;


    public override void Initialize() {
        gm = FindObjectOfType<GameManager>();
    }

    public override void OnGameSerieEnd() {
    }

    public override void OnGameStart() {
    }

    public override void OnGameEnd() {
    }

    public override IEnumerator PlayTurn()
    {
        while (!(pieceSelectionne != null && bCibleSelectionne)) {
            yield return null;
            //Debug.Log("En attente d'input ...");
            TrySelectionner();
        }

        gm.SetCurrentPlay(new Play(pieceSelectionne.GetPosition(), cibleSelectionne));
        ResetSelection();
    }

    private void TrySelectionner() {
        // Sélectionner une pièce
        if (pieceSelectionne == null) {
            //Debug.Log("En attente d'une pièce ...");
            TrySelectionnerPiece();

        } else {
            // Sélectionner une destination
            //Debug.Log("En attente d'une destination ...");
            TrySelectionnerDestination();
        }
    }

    private void TrySelectionnerPiece() {
        if (Input.GetMouseButtonDown(0))
        {
            //Ray ray = new Ray(humanCamera.transform.position, humanCamera.transform.forward);
            Ray ray = gm.gameCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit);

            // Il faut toucher une pièce
            if (hit.collider != null)
            {
                GameObject go = hit.transform.parent.gameObject;
                Piece piece = go.GetComponent<Piece>();
                if (piece != null && piece.color == color)
                {
                    SelectionnerPiece(piece);
                    return;
                }
            }

            // Si on a cliqué à côté, on déselectionne tout :)
            DeselectionnerPiece();
        }
    }

    private void SelectionnerPiece(Piece piece) {
        pieceSelectionne = piece;

        // Mettre les cases en couleur !
        List<Vector2Int> pos = piece.GetData().GetAllValidMoves();
        bool noPosAvailable = pos.Count == 0;
        foreach(Vector2Int p in pos) {
            GameObject tile = gm.board.tiles[p.x, p.y];
            if (gm.pieceManager.IsPiece(p) && gm.pieceManager.GetPiece(p).color != piece.color) {
                tile.GetComponent<Renderer>().material.color = captureColor;
            } else {
                tile.GetComponent<Renderer>().material.color = deplacementColor;
            }
        }

        // Si la pièce ne peut aller nul part, alors on la met en rouge !
        oldColor = pieceSelectionne.model.GetComponent<Renderer>().material.color;
        if(noPosAvailable) {
            pieceSelectionne.model.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    private void DeselectionnerPiece() {
        if (pieceSelectionne != null)
        {
            //Color color = pieceSelectionne.color == ChessColor.WHITE ? Color.white : Color.black;
            pieceSelectionne.model.GetComponent<Renderer>().material.color = oldColor;

            // Mettre les cases en couleur !
            List<Vector2Int> pos = pieceSelectionne.GetData().GetDestinationsAccessibles();
            foreach(Vector2Int p in pos) {
                if (gm.pieceManager.IsKingSafe(new Play(pieceSelectionne.GetPosition(), p))) {
                    GameObject tile = gm.board.tiles[p.x, p.y];
                    Material mat = ((p.x + p.y) % 2 == 0) ? gm.board.blackMaterial : gm.board.whiteMaterial;
                    tile.GetComponent<Renderer>().material = mat;
                }
            }

            pieceSelectionne = null;
        }
    }

    private void ResetSelection() {
        DeselectionnerPiece();
        bCibleSelectionne = false;
    }


    private void TrySelectionnerDestination() {
        if (Input.GetMouseButtonDown(0) && !bCibleSelectionne)
        {
            //Ray ray = new Ray(humanCamera.transform.position, humanCamera.transform.forward);
            Ray ray = gm.gameCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit);

            // Il faut toucher une pièce ou une case
            if (hit.collider != null)
            {
                GameObject go = hit.transform.parent.gameObject;
                Piece piece = go.GetComponent<Piece>();
                // On a cliqué sur la même pièce, donc on déselectionne !
                if (piece != null && piece == pieceSelectionne) {
                    DeselectionnerPiece();
                    return;
                }
                // Selectionner une autre pièce
                if (piece != null && piece != pieceSelectionne) {
                    if(piece.color == color) { // De la même couleur, on change de sélection
                        DeselectionnerPiece();
                        SelectionnerPiece(piece);
                        return;
                    } else { // D'une autre couleur, c'est donc là-bas que l'on veut aller !
                        SelectionnerDestination(piece.GetPosition());
                        return;
                    }
                }
                // Selectionner une case
                if (hit.collider.gameObject.transform.parent.gameObject.name == "Tile") {
                    Vector2Int pos = new Vector2Int((int)hit.transform.position.x, (int)hit.transform.position.z);
                    SelectionnerDestination(pos);
                    return;
                }
            }

            // Si on a cliqué à côté, bah on fait rien :D
        }
    }

    private void SelectionnerDestination(Vector2Int pos) {
        cibleSelectionne = pos;
        bCibleSelectionne = true;
    }
}
