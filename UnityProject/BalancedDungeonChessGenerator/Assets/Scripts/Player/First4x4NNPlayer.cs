﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public struct TrainingData {
    public NDarrayF input;
    public NDarrayF expectedOutput;
    public TrainingData(NDarrayF input, NDarrayF expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }
}

public class First4x4NNPlayer : Player {

    public int nbSimulations = 100;
    public string fileName = "MyNeuralNetworkModel";
    private string pathModel, pathDirectory;

    private GameManager gm;

    private MCTS mcts;
    private ModelSequential model;
    private Shape inputShape;
    private List<TrainingData> trainingDatas; // inputs, expected-outputs !

    public override void Initialize() {
        gm = FindObjectOfType<GameManager>();
        if(!fileName.EndsWith(".model"))
            fileName += ".model";
        pathDirectory = Application.persistentDataPath + "/NeuralNetworks/";
        pathModel = pathDirectory + fileName;

        // Si le modèle n'existe pas à ce nom-ci
        if (!File.Exists(pathModel)) {
        //if (true) {
            // Créer le réseau de neurone
            Debug.Log("Création du modèle " + fileName + " ...");
            model = new ModelSequential();

            // Créer l'input avec les x plans
            // Créer le ou les hiddens layers
            inputShape = gm.GetInputShape();
            model.Add(new InputLayer(inputShape));
            model.Add(new FlattenLayer());
            model.Add(new DenseLayer(200, activationFunction: new LeakyReLuActivation()));
            model.Add(new DenseLayer(200, activationFunction: new LeakyReLuActivation()));
            model.Add(new DenseLayer(gm.GetNbOutputs(), activationFunction: new LeakyReLuActivation())); // Créer l'output avec 2 valeurs, le départ et l'arrivée La dernière valeur de l'ouput est un flottant indiquant le pourcentage de chance de gagner // D'ailleurs la fonction d'activation ici n'a aucun intêret je crois :D
            model.Add(new SoftMaxLayer(indStart: 0, indEnd: gm.GetNbOutputs() - 1));

            // On compile avec les bons paramètres ! :)
            // ATTENTION : il faudra coder soit-même son optimizer, sa loss et ses metrics
            // pour que ce soit d'accord avec l'output (probas, victoire)
            //model.Compile(optimizer: "sgd", loss: "binary_crossentropy", metrics: new string[] { "accuracy" });

        }
        else {
            // Sinon charger le modèle de ce nom-ci
            Debug.Log("Chargement du modèle " + fileName + " ...");
            StreamReader sr = new StreamReader(pathModel);
            if (sr != null) {
                ModelSequentialData data = JsonUtility.FromJson<ModelSequentialData>(sr.ReadToEnd());
                model = ModelSequential.GetModelFromData(data);

            } else {
                throw new System.Exception("Echec de chargement du streamreader !" + pathModel);
            }
        }
    }

    public override void OnGameEnd() {
        // 0.00003f est la valeur qui fonctionne pour aucun hidden layer !
        // 0.000001f est la valeur qui fonctionne pour (200, 200, output, softmax)
        float learningRate = 0.000001f;
        int steps = 30;
        int batchSize = 5;

        // Puis entrainer le model !
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        Debug.Log("Début du training (NbDatas = " + trainingDatas.Count
                + " LearningRate = " + learningRate
                + " steps = " + steps
                + " batchSize = " + batchSize + ") ...");
        model.Train(trainingDatas, learningRate, steps, batchSize);
        Debug.Log("Training terminé ! <3 (" + (sw.ElapsedMilliseconds / (float)1000) + "s)");
    }


    public override void OnGameStart() {
        // Vider la liste de state/coup donné
        trainingDatas = new List<TrainingData>();
    }

    public override IEnumerator PlayTurn() {
        float debut = Time.realtimeSinceStartup;
        // Récupérer les x plans du state of the game
        GameState gs = gm.GetCurrentGameState();

        // Lancer le MCTS
        mcts = new MCTS(gs, fonctionDEvaluation, gm);
        mcts.Run(nbSimulations);

        // Retenir le state et le coup donné dans une liste
        trainingDatas.Add(new TrainingData(gs.GetState(), mcts.GetExpectedOuput()));

        // Puis on envoie le play !!!
        gm.SetCurrentPlay(mcts.GetBestPlay());

        float fin = Time.realtimeSinceStartup;
        gm.LogTime("DUREE DU TOUR = " + (fin - debut) + "s");

        yield return null;
    }

    private NDarrayF fonctionDEvaluation(GameState gs) {
        // Prédire l'output
        NDarrayF output = model.Predict(gs.GetState()); //, batch_size: 1, verbose: 1);

        // Si on veut juste renvoyer un tableau uniforme ...
        //NDarrayF output = new NDarrayF(gm.GetNbOutputs());
        //for(int i = 0; i < output.GetNbElements(); i++) {
        //    output[i] = 1.0f / (float)output.GetNbElements();
        //}

        return output;
    }

    public override void OnGameSerieEnd() {
        // Créer le répertoire s'il n'existe pas
        if (!Directory.Exists(pathDirectory)) {
            Directory.CreateDirectory(pathDirectory);
        }

        // Sauvegarder le modèle
        string json = JsonUtility.ToJson(model.GetData());
        StreamWriter sw = new StreamWriter(pathModel);
        if(sw != null) {
            sw.Write(json);
            sw.Close();
            Debug.Log("Modèle sauvegardé à : " + pathModel + "\nAu nom : " + fileName);
        } else {
            throw new System.Exception("Echec de chargement du streamwriter !" + pathModel);
        }
    }
}
