﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Play {
    public Vector2Int from, to;

    public Play(Vector2Int from_, Vector2Int to_) { from = from_; to = to_; }
    public override string ToString() {
        return "(" + from + ", " + to + ")";
    }
}

public abstract class Player : MonoBehaviour {

    public ChessColor color;

    public abstract void Initialize();

    public abstract void OnGameStart();

    public abstract IEnumerator PlayTurn();

    public abstract void OnGameEnd();

    public abstract void OnGameSerieEnd();
}
