﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlayer : Player {

    private GameManager gm;

    public override void Initialize() {
        gm = FindObjectOfType<GameManager>();
    }

    public override void OnGameSerieEnd() {
    }

    public override void OnGameStart() {
    }

    public override void OnGameEnd() {
    }

    public override IEnumerator PlayTurn() {
        // ATTENTION CECI N'EST PAS UNIFORME !!!

        List<PieceData> mesPieces = gm.pieceManager.GetAllPieceOfColor(color);
        PieceData maPiece = mesPieces[Random.Range(0, mesPieces.Count)];
        List<Vector2Int> positionsAccessibles = maPiece.GetDestinationsAccessibles();
        while (positionsAccessibles.Count <= 0) {
            maPiece = mesPieces[Random.Range(0, mesPieces.Count)];
            positionsAccessibles = maPiece.GetDestinationsAccessibles();
        }
        Vector2Int maCible = positionsAccessibles[Random.Range(0, positionsAccessibles.Count)];
        gm.SetCurrentPlay(new Play(maPiece.position, maCible));

        yield return null;
    }
}
