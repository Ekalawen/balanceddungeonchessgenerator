﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Keras;

public class GameManager : MonoBehaviour
{
    public string sceneName;
    public int nbGames = 1;
    public int nbMaxTurns = 50;
    public bool saveGames = true;
    public bool display = true;
    public float timeBetweenTurns = 0.5f;

    public bool printTimeLog = false;

    public Board board;
    public PieceManager pieceManager;
    public Player whitePlayer;
    public Player blackPlayer;

    public bool useHistory = false;
    public string historyPath;
    public GameObject historyUI;
    [HideInInspector]
    public History history;
    [HideInInspector]
    public Camera gameCamera;
    [HideInInspector]
    public VictoryManager victoryManager;
    [HideInInspector]
    public int turnNumber = 0;
    [HideInInspector]
    public Player currentPlayer;

    private Play currentPlay;

    void Start() {
        //TestModelSequential tms = new TestModelSequential();
        //tms.Run();
        if (display)
            InitializeCamera();
        if (useHistory)
        {
            InitializeHistoryGame();
        }
        else
        {
            InitializeNormalGame();
        }
    }

    private void InitializeNormalGame() {
        historyUI.SetActive(false);
        board.Initialize();

        whitePlayer.Initialize();
        blackPlayer.Initialize();

        StartCoroutine(StartGameSerie());
    }

    private IEnumerator StartGameSerie() {
        victoryManager = new VictoryManager(this);
        for (int i = 0; i < nbGames; i++) {
            Debug.Log("Game n°" + (i + 1) + " :");

            pieceManager.Initialize();
            pieceManager.GeneratePieces();

            history = GetComponent<History>();
            history.SaveFirstState();

            yield return StartGame();

            if(i + 1 != nbGames)
                pieceManager.Clean();
        }
        victoryManager.PrintRatios();

        whitePlayer.OnGameSerieEnd();
        blackPlayer.OnGameSerieEnd();
    }

    private void InitializeHistoryGame() {
        historyUI.SetActive(true);
        history = GetComponent<History>();
        history.LoadHistory(historyPath);

        board.Initialize();
        pieceManager.Initialize();

        pieceManager.LoadFromHistory();
        victoryManager = new VictoryManager(this);

        // S'occuper du HUD !
        // S'occuper des joueurs
        // Faire en sorte de ne pas sauvegarder la partie que l'on va jouer ! x)

        history.PlayUntilEnd();
    }

    private IEnumerator StartGame() {
        yield return null; // On rend la main tout de suite pour pouvoir sortir du Start() !

        turnNumber = 0;
        currentPlayer = whitePlayer;
        whitePlayer.OnGameStart();
        blackPlayer.OnGameStart();
        while (!victoryManager.IsOver(currentPlayer.color)) {
            yield return GetPlay(currentPlayer); // Setup currentPlay

            SavePlayInHistory();

            pieceManager.Play(currentPlay);

            currentPlayer = SwapCurrentPlayer(currentPlayer);
            turnNumber++;
        }
        whitePlayer.OnGameEnd();
        blackPlayer.OnGameEnd();

        victoryManager.PrintVictory();

        if(saveGames)
            history.SaveHistory();
    }

    private Player SwapCurrentPlayer(Player currentPlayer) {
        return (currentPlayer == whitePlayer) ? blackPlayer : whitePlayer;
    }

    private IEnumerator GetPlay(Player player) {
        yield return player.PlayTurn(); // Setup currentPlay
        while (!board.IsValid(currentPlay, player))
        {
            yield return player.PlayTurn(); // Setup currentPlay
        }
        yield return null;
    }

    public void SetCurrentPlay(Play play) {
        currentPlay = play;
    }

    private void InitializeCamera() {
        GameObject go = Instantiate(new GameObject());
        gameCamera = go.AddComponent<Camera>();
        go.AddComponent<CameraController>();
    }

    private void SavePlayInHistory() {
        PieceData capturedPiece = pieceManager.GetPiece(currentPlay.to);
        history.SaveNextState(new HistoryPlay(currentPlay, capturedPiece));
    }

    public void HistoryPlay() {
        history.PlayUntilEnd();
    }
    public void HistoryStop() {
        history.StopPlaying();
    }
    public void HistoryNext() {
        history.PlayNextPlay();
    }
    public void HistoryPrev() {
        history.UndoLastPlay();
    }
    public void HistoryGoToStart() {
        history.GotoStart();
    }
    public void HistoryGoToEnd() {
        history.GotoEnd();
    }

    public Shape GetInputShape() {
        int N = board.size;
        int nbTiles = TileType.GetNames(typeof(TileType)).Length;
        int M = pieceManager.whitePiecesModels.Count + pieceManager.blackPiecesModels.Count + nbTiles;
        // On aimerait également passer en argument :
        // la couleur du joueur qui doit jouer
        // le tour en cours
        // D'où le + 2 !
        return new Shape(N, N, M + 2);
    }

    // Première version simple : on code le déplacement avec une case de départ et une case d'arrivée
    // Deuxième version un peu opti : on code le déplacement avec une case de départ et les mouvements de la reine+cavalier
    public int GetNbOutputs() {
        int N = board.size;
        // On rajoute 1 output pour la probabilité de gagner à partir de cette position ! :)
        return (N * N) * (N * N) + 1;
    }

    public Play GetPlayFromOuputs(int output) {
        // Vérifier cette fonction !!!
        int N = board.size;
        int departIndice = output / (N * N);
        int arriveeIndice = output % (N * N);
        Vector2Int depart = new Vector2Int(departIndice % N, departIndice / N);
        Vector2Int arrivee = new Vector2Int(arriveeIndice % N, arriveeIndice / N);
        return new Play(depart, arrivee);
    }

    public int GetIndiceOutputsFromPlay(Play play) {
        // Vérifier cette fonction !!!
        int N = board.size;
        int indiceStart = play.from.x + play.from.y * N;
        int indiceEnd = play.to.x + play.to.y * N;
        return indiceStart * (N * N) + indiceEnd;
    }

    public ChessColor OtherColor(ChessColor color) {
        return color == ChessColor.WHITE ? ChessColor.BLACK : ChessColor.WHITE;
    }

    public GameState GetCurrentGameState() {
        int N = board.size;
        int nbWhitePieces = pieceManager.whitePiecesModels.Count;
        int nbBlackPieces = pieceManager.blackPiecesModels.Count;
        int nbTileType = TileType.GetNames(typeof(TileType)).Length;

        // On initialise un GameState de la bonne taille entièrement vide
        GameState gs = new GameState(pieceManager.GetPiecesDatasCopy(),
                                     N,
                                     nbWhitePieces,
                                     nbBlackPieces,
                                     nbTileType,
                                     currentPlayer.color,
                                     turnNumber,
                                     this);

        return gs;
    }

    public void LogTime(string str) {
        if (printTimeLog)
            Debug.Log(str);
    }
}
