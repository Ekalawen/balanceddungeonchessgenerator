# BalancedDungeonChessGenerator

The goal of this programme is to create a new asymetric Chess board with alternative rules which is still balanced between the two players.
The plan is to use a Generative Adversarial Networks paired with AlphaZero algorithme as the evaluation function to achieve this task.

The project is subdivised into 2 others projects :
- a Unity project as a way to visualize the game
- a Python with TensorFlow project as a way to use Neural Networks